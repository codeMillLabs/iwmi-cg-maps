/*
 * FILENAME
 *     ProjectOutcomeQueryDaoTest.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.cgmaps.model.ProjectOutcomeView;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test class for {@link ProjectOutcomeQueryDao}.
 * </p>
 * <p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
public class ProjectOutcomeQueryDaoTest {

	@Autowired
	private ProjectOutcomeQueryDao projectOutcomeQueryDao;

	@Test
	public void testFetchProjectOutcomeView() {
		List<ProjectOutcomeView> projectOutcomeViews = projectOutcomeQueryDao
				.fetchProjectOutcomeListView(null);

		assertTrue(!projectOutcomeViews.isEmpty());
	}

}
