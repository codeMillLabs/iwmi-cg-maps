/**
 * 
 */
package com.hashcode.iwmi.cgmaps.dao;

import static com.hashcode.iwmi.cgmaps.services.SearchCriteriaType.PROJECT_SEARCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.cgmaps.dao.impl.ProjectInfoQueryDaoImpl;
import com.hashcode.iwmi.cgmaps.model.ProjectInfoView;
import com.hashcode.iwmi.cgmaps.model.ProjectListView;
import com.hashcode.iwmi.cgmaps.services.AdvancedSearchCriteria;
import com.hashcode.iwmi.cgmaps.services.ProjectListCriteria;

/**
 * <p>
 * Test class for {@link ProjectInfoQueryDaoImpl}.
 * </p>
 * 
 * @author Amila Silva
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:test-context.xml"
})
public class ProjectInfoQueryDaoTest
{

    @Autowired
    private ProjectInfoQueryDao projectInfoQueryDao;

    @Test
    public void testFetchProjectListViewSearchWithCriteria()
    {

        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setCountry("Sri Lanka");

        List<ProjectListView> projectListViews = projectInfoQueryDao.fetchProjectListView(criteria);

        assertTrue(!projectListViews.isEmpty());
        assertEquals(12, projectListViews.size());
    }

    @Test
    public void testFetchProjectListViewSearchWithEmptyData()
    {
        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setCountry("Colombo");

        List<ProjectListView> projectListViews = projectInfoQueryDao.fetchProjectListView(criteria);

        assertTrue(projectListViews.isEmpty());
    }

    @Test
    public void testFetchProjectListViewSearchWithDateRange()
    {
        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setCountry("India");
        criteria.setFromYear("2010-01-01");
        criteria.setToYear("2010-12-31");

        List<ProjectListView> projectListViews = projectInfoQueryDao.fetchProjectListView(criteria);

        assertTrue(!projectListViews.isEmpty());
        assertEquals(11, projectListViews.size());
    }

    @Test
    public void testFetchAllCountries()
    {

        List<String> countries = projectInfoQueryDao.fetchAllAvailbaleCountries();
        assertTrue(!countries.isEmpty());
    }

    @Test
    public void testFetchPrograms()
    {
        List<String> programs = projectInfoQueryDao.fetchAllPrograms();
        assertTrue(!programs.isEmpty());
    }

    @Test
    public void testAdvanceSearchCriteria1()
    {
        AdvancedSearchCriteria criteria = new AdvancedSearchCriteria();
        criteria.setSearchCriteriaType(PROJECT_SEARCH);
        criteria.setTextSearch("Small Dam");

        List<ProjectInfoView> projectInfos = projectInfoQueryDao.fetchProjectInfo(criteria);

        assertTrue(!projectInfos.isEmpty());
        assertTrue(projectInfos.size() == 1);
    }

    @Test
    public void testAdvanceSearchCriteria2()
    {
        AdvancedSearchCriteria criteria = new AdvancedSearchCriteria();
        criteria.setSearchCriteriaType(PROJECT_SEARCH);
        criteria.setTextSearch("Small Dam");
        criteria.getCountries().add("India");
        criteria.getCountries().add("Sri Lanka");
        criteria.getCountries().add("Morocco");

        List<ProjectInfoView> projectInfos = projectInfoQueryDao.fetchProjectInfo(criteria);

        assertTrue(!projectInfos.isEmpty());
        assertTrue(projectInfos.size() == 1);
    }

    @Test
    public void testGetProjectStartedYr()
    {
        List<Integer> projStartedYr = projectInfoQueryDao.getProjectStartedYr();

        assertTrue(!projStartedYr.isEmpty());
    }
    
    @Test
    public void testGetCountriesForRegion() {
        List<String> regions = new ArrayList<String>();
        regions.add("South-Eastern Asia");
        regions.add("Eastern Africa");
        regions.add("Southern Asia");
        
        List<String> countries = projectInfoQueryDao.fetchCountriesForRegion(regions);
        
        assertTrue(!countries.isEmpty());
        assertTrue(countries.size() == 35);
    }

    @Test
    public void testGetCountriesForEmptyRegion() {
        List<String> regions = new ArrayList<String>();
        
        List<String> countries = projectInfoQueryDao.fetchCountriesForRegion(regions);
        
        assertTrue(!countries.isEmpty());
        assertTrue(countries.size() == 82);
    }
    
    @Test
    public void testGetCountriesForOneRegion() {
        List<String> regions = new ArrayList<String>();
        regions.add("Southern Asia");
        
        List<String> countries = projectInfoQueryDao.fetchCountriesForRegion(regions);
        
        assertTrue(!countries.isEmpty());
        assertTrue(countries.size() == 9);
    }
}
