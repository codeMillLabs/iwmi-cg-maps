/**
 * 
 */
package com.hashcode.iwmi.cgmaps.service;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.cgmaps.services.ProjectDetailQueryService;
import com.hashcode.iwmi.cgmaps.services.ProjectListCriteria;

/**
 * <p>
 * Test class for {@link ProjectDetailQueryService}.
 * </p>
 * 
 * @author Amila Silva
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
public class ProjectDetailQueryServiceTest {

	private static final Logger logger = LoggerFactory
			.getLogger(ProjectDetailQueryServiceTest.class);

	@Autowired
	private ProjectDetailQueryService projectDetailQueryService;

	@Test
	public void shouldGetProjectDistributionOnCountriesNoCriteria() {
		logger.info("shouldGetProjectDistributionOnCountriesNoCriteria - Test Start");

		Map<String, Double> prjDistrb = projectDetailQueryService
				.getProjectDistributionOnCountries(null);
		assertTrue(!prjDistrb.isEmpty());
		logger.info("shouldGetProjectDistributionOnCountriesNoCriteria - Test End");
	}

	@Test
	public void shouldGetProjectDistributionOnCountriesWithCriteria() {
		logger.info("shouldGetProjectDistributionOnCountriesWithCriteria - Test Start");

		ProjectListCriteria criteria = new ProjectListCriteria();
		criteria.setCountry("Sri Lanka");
		Map<String, Double> prjDistrb = projectDetailQueryService
				.getProjectDistributionOnCountries(criteria);

		assertTrue(!prjDistrb.isEmpty());
		logger.info("shouldGetProjectDistributionOnCountriesWithCriteria - Test End");
	}
	
	@Test
	public void shouldGetProjectDistributionOnCountriesWithCriteriaEmpty() {
		logger.info("shouldGetProjectDistributionOnCountriesWithCriteriaEmpty - Test Start");

		ProjectListCriteria criteria = new ProjectListCriteria();
		criteria.setCountry("Colombo");
		Map<String, Double> prjDistrb = projectDetailQueryService
				.getProjectDistributionOnCountries(criteria);

		assertTrue(prjDistrb.isEmpty());
		logger.info("shouldGetProjectDistributionOnCountriesWithCriteriaEmpty - Test End");
	}

}
