<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<link rel="icon" type="image/png" href="./resources/images/favicon.png" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="library.jsp" />
<title>International Water Management Institute (IWMI)</title>
</head>
<body>
	<div id="page_wrapper">
		<div id="header-style">
			<tiles:insertAttribute name="header-content" />
		</div>
		<div id="menubar-style">
			<h2>
				<tiles:insertAttribute name="menubar-content" />
			</h2>
		</div>
		<%-- <div id="subtitle-style">
			<tiles:insertAttribute name="subtitle-content" />
		</div> --%>
		<div id="primary-style">
			<tiles:insertAttribute name="primary-content" />
		</div>
		<div id="footer-style">
			<tiles:insertAttribute name="footer-content" />
		</div>
	</div>
</body>
</html>