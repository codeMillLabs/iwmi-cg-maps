<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div id="map-container">
	<div class="view-menu">
		<a href="map-view">Map View</a> | <a href="list-view">List
			View</a>
	</div>
	<div id="map-canvas">
		<!-- Left side panel -->
		<table style="width: 100%">
			<c:if test="${not empty projectList}">
				<c:forEach var="prj" items="${projectList}">
					<tr>
						<%
							String navi2 = request.getParameter("navi");
									if (null == navi2) {
										navi2 = "1";
									}
						%>
						<td><div class="list-view-item">
								<a
										href="./prj-details?navi=<%=navi2%>&prjId=${prj.projectId}">${prj.projectId}:${prj.program}
										: ${prj.theme} <br /> ${prj.webTitle}
									</a>
							</div>
							<hr /></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>
	</div>
	<div id="right-side-panel">
		<form:form action="list-view" method="post"
			modelAttribute="projListModel">
			<div>
				<p></p>
				Refine by Research Program<br>
				<form:select path="program">
					<form:option value="">All</form:option>
					<form:options items="${programList}" />
				</form:select>
			</div>
			<div>
				<br>Refine by year<br>
				<form:select path="year">
					<form:option value="">All</form:option>
					<form:options items="${projectYrs}" />
				</form:select>
				<br>
			</div>
			<div>
				<br>Refine by country<br>
				<form:select path="country">
					<form:option value="">All</form:option>
					<form:options items="${countryList}" />
				</form:select>
				<br>
				<br> <input class="search-button" type="submit" value="" /> <br>
				<div id="search-summary"></div>
			</div>
		</form:form>
	</div>

	<!-- Right side panel -->
	<div></div>

</div>
