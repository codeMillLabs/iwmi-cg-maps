<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>

<%@page import="com.hashcode.iwmi.cgmaps.model.ProjectInfoView"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div class="details-border">
		<%
			ProjectInfoView projectInfoView = (ProjectInfoView) request
					.getAttribute("prjDetail");
		%>
		<div id="tabs">
			<ul>
				<li><a href="#tabs-1">Project Snapshot</a></li>
				<!-- <li><a href="#tabs-2">Project Outputs</a></li> -->
			</ul>
			<div id="tabs-1">
				<div id="project-name">${prjDetail.webTitle}</div>
				<hr />
				<br />
				<div class="prg-detail-65">
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Project Name</div>
						<div class="prg-detail-content-body">${prjDetail.longName}</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Location</div>
						<div class="prg-detail-content-body">${prjDetail.locationStr}</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Timeline</div>
						<div class="prg-detail-content-body">
							Start Date :
							<fmt:formatDate value="${prjDetail.startDate}"
								pattern="yyyy-MM-dd" />
							| End Date :
							<fmt:formatDate value="${prjDetail.endDate}" pattern="yyyy-MM-dd" />

							<c:if test="${not empty prjDetail.extendedDate}">
						| Extended Date : <fmt:formatDate
									value="${prjDetail.extendedDate}" pattern="yyyy-MM-dd" />
							</c:if>
						</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Description</div>
						<div class="prg-detail-content-body">${prjDetail.description}</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Project Name</div>
						<div class="prg-detail-content-body">${prjDetail.longName}</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Web Address</div>
						<div class="prg-detail-content-body">
							<%
								if (null != projectInfoView.getWebAddress()
										&& !projectInfoView.getWebAddress().equals("")) {
							%><a href="${prjDetail.webAddress}" target="_blank">${prjDetail.webAddress}</a>
							<%
								}
							%>
						</div>
					</div>
				</div>
				<div class="prg-detail-33">
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Research Program</div>
						<div class="prg-detail-content-body">${prjDetail.program}</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Theme</div>
						<div class="prg-detail-content-body">${prjDetail.theme}</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Financing Sources</div>
						<div class="prg-detail-content-body">${prjDetail.donorsStr}</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Project Leader</div>
						<div class="prg-detail-content-body">
							${prjDetail.projectLeader}
							<%
								if (null != projectInfoView.getPlEmail()
										&& !projectInfoView.getPlEmail().equals("")) {
							%>(${prjDetail.plEmail})<%
								}
							%>
						</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Project Contact</div>
						<div class="prg-detail-content-body">
							${prjDetail.contactName}
							<%
								if (null != projectInfoView.getContactEmail()
										&& !projectInfoView.getContactEmail().equals("")) {
							%>(${prjDetail.contactEmail})<%
								}
							%>
						</div>
					</div>
					<div class="prg-detail-content-row">
						<div class="prg-detail-content-topic">Status</div>
						<div class="prg-detail-content-body">${prjDetail.projectStatus}</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<%-- <div id="tabs-2">
				<div>
					<c:if test="${hasOutcomeData}"></c:if>
					<div class="prg-detail-content-row">
						<table class="gridtable">
							<tr>
								<td colspan="4"><div id="project-name">
										${prjDetail.program} : ${prjDetail.theme} <br />${prjDetail.webTitle}
									</div></td>
							</tr>
							<tr>
								<th>Name</th>
								<th width="80px">Due Date</th>
								<th>Responsible</th>
								<th>Status</th>
							</tr>
							<c:forEach var="outcome" items="${prjOutcome}">
								<tr>
									<td>${outcome.name}</td>
									<td>${outcome.dueDate}</td>
									<td>${outcome.responsible}</td>
									<td>${outcome.status}</td>
								</tr>
							</c:forEach>

						</table>
					</div>
				</div>

			</div> --%>
		</div>
	</div>
	<div class="clear"></div>
	<div class="backButton">
		<a href="JavaScript:void(o);" onclick="history.go(-1)"></a>
	</div>
	<div class="clear"></div>
	</div>