<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div id="view-menu">
		<a href="advn-project-form?navi=2">Search Projects</a> / <a
			href="advn-outcome-form?navi=2">Search Outputs</a>
	</div>
	<div id="search-form">
		<form:form action="advn-outcome-form" method="post"
			modelAttribute="advSerFrmModel">
			<table>
				<tr>
					<td><form:label path="startDate">MTP :</form:label></td>
					<td><form:select path="startDate">
							<form:option value="">All</form:option>
						</form:select></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
				<tr>
					<td><form:label path="programs">Programs</form:label></td>
					<td><form:select path="programs">
							<form:option value="">All</form:option>
							<form:options items="${programItmes}" />
						</form:select></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
				<tr>
					<td><form:label path="regions">Regions</form:label></td>
					<td><form:select path="regions">
							<form:option value="">All</form:option>
							<form:options items="${regionItems}" />
						</form:select></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
				<tr>
					<td><form:label path="countries">Countries</form:label></td>
					<td><form:select path="countries">
							<form:option value="">All</form:option>
							<form:options items="${countryItems}" />
						</form:select></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
				<tr>
					<td><form:label path="textSearch">Text Search</form:label></td>
					<td><form:input path="textSearch" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Search" />
						&nbsp;&nbsp; <input type="reset" value="Clear" /></td>
				</tr>
			</table>

		</form:form>
	</div>

</div>
