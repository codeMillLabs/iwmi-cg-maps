<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div id="map-container">
	<div id="view-menu">
		<a href="ongoing-map-view?navi=2">Map View</a>
		/
		<a href="ongoing-prj?navi=2">Project List View</a>
	</div>
	<div id="map-canvas">
		<!-- Left side panel -->
		<table style="width: 100%">
			<c:if test="${not empty projectList}">
				<c:forEach var="prj" items="${projectList}">
					<tr>
						<td><a href="./prj-details?navi=2&prjId=${prj.projectId}">${prj.projectId}:
								${prj.program} : ${prj.theme} <br /> ${prj.webTitle}
						</a>
						<hr/>
						</td>
					</tr>
				</c:forEach>
			</c:if>
		</table>
	</div>
	<div id="right-side-panel">
		<form:form action="ongoing-prj" method="post"
			modelAttribute="ongoingProjModel">
			<div>
				<p></p>
				Refine by Research Program<br>
				<form:select path="program">
					<form:option value="">All</form:option>
					<form:options items="${programList}" />
				</form:select>
			</div>
			<div>
				<br>Refine by year<br>
				<form:select path="year">
					<form:option value="">All</form:option>
					<form:options items="${projectYrs}" />
				</form:select>
				<br>
			</div>
			<div>
				<br>Refine by country<br>
				<form:select path="country">
					<form:option value="">All</form:option>
					<form:options items="${countryList}" />
				</form:select>
				<br><br>
				<input type="submit" value="Search" />
				<br>
				<div id="search-summary"></div>
			</div>
		</form:form>
	</div>

	<!-- Right side panel -->
	<div></div>

</div>
