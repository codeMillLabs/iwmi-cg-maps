<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>
<script type="text/javascript"
	src="https://maps.google.com/maps/api/js?sensor=false"></script>

<script type="text/javascript">
	var colors = [ '#FF0000', '#00FF00', '#0000FF', '#FFFF00' ];
	var map;
	var dict = {};
	var dataCache;

	var opacityMap = {};

	function initialize() {
		constructOpacityMap();
		var myOptions = {
			center : new google.maps.LatLng(0, 0),
			zoom : 2,
			minZoom : 2,
			maxZoom : 4,
			mapTypeControl : false,
			navigationControl : true,
			scaleControl : true,
			scrollwheel : false,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById('map-canvas'),
				myOptions);

		// Initialize JSONP request
		var script = document.createElement('script');
		var url = [ 'https://www.googleapis.com/fusiontables/v1/query?' ];
		url.push('sql=');
		var query = 'SELECT name, kml_4326 FROM '
				+ '1foc3xO9DyfSIF6ofvN0kp2bxSfSeKog5FbdWdQ';
		var encodedQuery = encodeURIComponent(query);
		url.push(encodedQuery);
		url.push('&callback=drawMap');
		url.push('&key=AIzaSyAm9yWCV7JPCTHCJut8whOjARd7pwROFDQ');
		script.src = url.join('');
		var body = document.getElementsByTagName('body')[0];
		body.appendChild(script);
	}

	function redraw() {
		var myOptions = {
			center : new google.maps.LatLng(0, 0),
			zoom : 2,
			minZoom : 2,
			maxZoom : 4,
			mapTypeControl : false,
			navigationControl : true,
			scaleControl : true,
			scrollwheel : false,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById('map-canvas'),
				myOptions);
		drawPolygons(dataCache);
	}

	function drawMap(data) {
		dataCache = data;
		filterUpdateMap();
	}

	function drawPolygons(data) {
		dataCache = data;
		var rows = data['rows'];
		/* var resultString; */
		for ( var i in rows) {
			var countryName = rows[i][0];
			/* resultString = resultString + countryName+ ","; */
			if (countryName != 'Antarctica') {
				var newCoordinates = [];
				var geometries = rows[i][1]['geometries'];
				if (geometries) {
					for ( var j in geometries) {
						newCoordinates
								.push(constructNewCoordinates(geometries[j]));
					}
				} else {
					newCoordinates = constructNewCoordinates(rows[i][1]['geometry']);
				}
				var country = new google.maps.Polygon({
					paths : newCoordinates,
					strokeColor : '#ff9900',
					strokeOpacity : 0,
					strokeWeight : 1,
					fillColor : '#ff9900',
					fillOpacity : 1
				});

				var hashValue = generateHashValue(country.getPath().getAt(0)
						.lat(), country.getPath().getAt(0).lng(), country
						.getPath().getAt(1).lat(), country.getPath().getAt(1)
						.lng());

				dict[hashValue] = countryName;
				
				country.setOptions({
					fillColor : '#ff9900',
					fillOpacity : getOpacityValue(countryName)
				});

				google.maps.event.addListener(country, 'mouseover', function() {
					var hashValue = generateHashValue(this.getPath().getAt(0)
							.lat(), this.getPath().getAt(0).lng(), this
							.getPath().getAt(1).lat(), this.getPath().getAt(1)
							.lng());
					if (getOpacityValue(dict[hashValue]) > 0) {
						this.setOptions({
							fillColor : '#00ff00',
							fillOpacity : 1
						});
					}
				});

				google.maps.event.addListener(country, 'mouseout', function() {
					var hashValue = generateHashValue(this.getPath().getAt(0)
							.lat(), this.getPath().getAt(0).lng(), this
							.getPath().getAt(1).lat(), this.getPath().getAt(1)
							.lng());
					this.setOptions({
						fillColor : '#ff9900',
						fillOpacity : getOpacityValue(dict[hashValue])
					});
				});
				google.maps.event
						.addListener(
								country,
								'click',
								function(event) {
									var hashValue = generateHashValue(this
											.getPath().getAt(0).lat(), this
											.getPath().getAt(0).lng(), this
											.getPath().getAt(1).lat(), this
											.getPath().getAt(1).lng());
									var country = dict[hashValue];
									if (getOpacityValue(dict[hashValue]) > 0) {
									var center = $('#center').val();
									var year = $('#year').val();
									country = checkCountryName(country);
									$
											.colorbox({

												href : "ajax/map-country-country-details?program="
														+ center
														+ "&year="
														+ year
														+ "&country="
														+ country
											});
									}
								});

				country.setMap(map);
			}
		}
		/* var resultArray = resultString.split(",");
		resultArray.sort();
		resultString = "";
		for (var i=0;i<resultArray.length;i++)
		{
			resultString = resultString + "&lt;option value=&quot;"+resultArray[i]+"&quot;&gt;"+resultArray[i]+"&lt;/option&gt;<br/>";
		}
		document.getElementById("output-canvas").innerHTML = resultString; */
	}

	function constructNewCoordinates(polygon) {
		var newCoordinates = [];
		var coordinates = polygon['coordinates'][0];
		for ( var i in coordinates) {
			newCoordinates.push(new google.maps.LatLng(coordinates[i][1],
					coordinates[i][0]));
		}
		return newCoordinates;
	}

	function generateHashValue(lat1, lng1, lat2, lng2) {
		return lat1 + "|" + lng1 + "|" + lat2 + "|" + lng2;
	}

	function constructOpacityMapFromList(list) {
		constructOpacityMap();
		var splittedArray = list.split("|");

		for ( var i = 0; i < splittedArray.length; i++) {
			var splittedValue = splittedArray[i].split("_");
			opacityMap[splittedValue[0]] = splittedValue[1];
		}

		return opacityMap;
	}

	function constructOpacityMap() {
		opacityMap['Afghanistan'] = 0;
		opacityMap['Albania'] = 0;
		opacityMap['Algeria'] = 0;
		opacityMap['Andorra'] = 0;
		opacityMap['Angola'] = 0;
		opacityMap['Anguilla'] = 0;
		opacityMap['Antarctica'] = 0;
		opacityMap['Argentina'] = 0;
		opacityMap['Australia'] = 0;
		opacityMap['Azerbaijan'] = 0;
		opacityMap['Bahamas'] = 0;
		opacityMap['Bangladesh'] = 0;
		opacityMap['Benin'] = 0;
		opacityMap['Bolivia'] = 0;
		opacityMap['Botswana'] = 0;
		opacityMap['Brazil'] = 0;
		opacityMap['Brunei'] = 0;
		opacityMap['Burkina Faso'] = 0;
		opacityMap['Burundi'] = 0;
		opacityMap['Cambodia'] = 0;
		opacityMap['China'] = 0;
		opacityMap['Colombia'] = 0;
		opacityMap['Congo (Kinshasa)'] = 0;
		opacityMap['Ecuador'] = 0;
		opacityMap['Egypt'] = 0;
		opacityMap['Ethiopia'] = 0;
		opacityMap['Ghana'] = 0;
		opacityMap['India'] = 0;
		opacityMap['Indonesia'] = 0;
		opacityMap['Iran'] = 0;
		opacityMap['Iraq'] = 0;
		opacityMap['Israel'] = 0;
		opacityMap['Jordan'] = 0;
		opacityMap['Kazakhstan'] = 0;
		opacityMap['Kenya'] = 0;
		opacityMap['Kyrgyzstan'] = 0;
		opacityMap['Laos'] = 0;
		opacityMap['Lesotho'] = 0;
		opacityMap['Malawi'] = 0;
		opacityMap['Malaysia'] = 0;
		opacityMap['Mali'] = 0;
		opacityMap['Morocco'] = 0;
		opacityMap['Mozambique'] = 0;
		opacityMap['Namibia'] = 0;
		opacityMap['Nepal'] = 0;
		opacityMap['Niger'] = 0;
		opacityMap['Nigeria'] = 0;
		opacityMap['Oman'] = 0;
		opacityMap['Pakistan'] = 0;
		opacityMap['Rwanda'] = 0;
		opacityMap['Senegal'] = 0;
		opacityMap['Sierra Leone'] = 0;
		opacityMap['South Africa'] = 0;
		opacityMap['Sri Lanka'] = 0;
		opacityMap['Sudan'] = 0;
		opacityMap['Swaziland'] = 0;
		opacityMap['Sweden'] = 0;
		opacityMap['Syria'] = 0;
		opacityMap['Tajikistan'] = 0;
		opacityMap['Tanzania'] = 0;
		opacityMap['Thailand'] = 0;
		opacityMap['Togo'] = 0;
		opacityMap['Tunisia'] = 0;
		opacityMap['Turkey'] = 0
		opacityMap['Turkmenistan'] = 0;
		opacityMap['Uganda'] = 0;
		opacityMap['Uzbekistan'] = 0;
		opacityMap['Vietnam'] = 0;
		opacityMap['Zambia'] = 0;
		opacityMap['Zimbabwe'] = 0;
	}
	
	function checkCountryName(countryName)
	{
		if(countryName == "Kyrgyzstan")
        {
            return  "Kyrgyz Republic";
        }
        else if(countryName == "Syria")
        {
            return  "Syrian Arab Republic";
        }
        else if(countryName  == "Andorra")
        {
            return  "Andorra";
        }
        else if(countryName == "Vietnam")
        {
            return  "VietNam";
        }
        else if(countryName == "Brunei")
        {
            return  "Brunei Darussalam";
        }
        else if(countryName == "Antarctica")
        {
            return  "Antarctica (the territory South of 60 deg S)";
        }
        else if(countryName == "Anguilla")
        {
            return  "Anguilla";
        }
        else if(countryName == "Congo (Kinshasa)")
        {
            return  "Congo";
        }
        else if(countryName == "Laos")
        {
            return  "Lao People's Democratic Republic";
        }
		
		return countryName;
	}
	
	
	function getOpacityValue(countryName) {
		if (typeof opacityMap[countryName] === "undefined") {
			return 0;
		} else {
			return opacityMap[countryName];
		}
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-container">
	<div id="map-canvas"></div>
	<div id="right-side-panel">
		<div align="left" id="project_filters">
			<div class="ui-corner-all h2N">
				<p></p>
				Refine by Research Program<br> <select
					onchange="filterUpdateMap();" name="center" id="center">
					<option value="All">All</option>
					<option value="AFRICA RICE">Africa Rice Center(AfricaRice)</option>
					<option value="BIOVERSITY">Bioversity International</option>
					<option value="CIAT">CIAT</option>
					<option value="CIFOR">CIFOR</option>
					<option value="CIMMYT">CIMMYT</option>
					<option value="CIP">CIP</option>
					<option value="CPWF">CP on Water and Food</option>
					<option value="GCP">Generation CP</option>
					<option value="HPCP">HarvestPlus CP</option>
					<option value="ICARDA">ICARDA</option>
					<option value="ICRISAT">ICRISAT</option>
					<option value="IFPRI">IFPRI</option>
					<option value="IITA">IITA</option>
					<option value="ILRI">ILRI</option>
					<option value="IRRI">IRRI</option>
					<option value="IWMI">IWMI</option>
					<option value="SSACP">Sub-Saharan Africa CP</option>
					<option value="WORLD AGROFORESTRY">World Agroforestry
						Centre</option>
					<option value="WORLDFISH">WorldFish Center</option>
				</select>
			</div>
			<div>
				<br>Refine by period<br> <select
					onchange="filterUpdateMap();" name="year" id="year">
					<option value="All">All</option>
					<option value="2011-2013">2011-2013</option>
					<option value="2010-2012">2010-2012</option>
					<option value="2009-2011">2009-2011</option>
				</select> <br>
			</div>
			<div>
				<br>Refine by country<br> <select
					onchange="filterUpdateMap();" name="country" id="country">
					<option value="All">All</option>
					<option value="Afghanistan">Afghanistan</option>
					<option value="Albania">Albania</option>
					<option value="Algeria">Algeria</option>
					<option value="Andorra">Andorra</option>
					<option value="Angola">Angola</option>
					<option value="Anguilla">Anguilla</option>
					<option value="Antarctica (the territory South of 60 deg S)">Antarctica</option>
					<option value="Argentina">Argentina</option>
					<option value="Australia">Australia</option>
					<option value="Azerbaijan">Azerbaijan</option>
					<option value="Bahamas">Bahamas</option>
					<option value="Bangladesh">Bangladesh</option>
					<option value="Benin">Benin</option>
					<option value="Bolivia">Bolivia</option>
					<option value="Botswana">Botswana</option>
					<option value="Brazil">Brazil</option>
					<option value="Brunei Darussalam">Brunei Darussalam</option>
					<option value="Burkina Faso">Burkina Faso</option>
					<option value="Burundi">Burundi</option>
					<option value="Cambodia">Cambodia</option>
					<option value="China">China</option>
					<option value="Colombia">Colombia</option>
					<option value="Congo">Congo</option>
					<option value="Ecuador">Ecuador</option>
					<option value="Egypt">Egypt</option>
					<option value="Ethiopia">Ethiopia</option>
					<option value="Ghana">Ghana</option>
					<option value="India">India</option>
					<option value="Indonesia">Indonesia</option>
					<option value="Iran">Iran</option>
					<option value="Iraq">Iraq</option>
					<option value="Israel">Israel</option>
					<option value="Jordan">Jordan</option>
					<option value="Kazakhstan">Kazakhstan</option>
					<option value="Kenya">Kenya</option>
					<option value="Kyrgyz Republic">Kyrgyz Republic</option>
					<option value="Lao People's Democratic Republic">Laos</option>
					<option value="Lesotho">Lesotho</option>
					<option value="Malawi">Malawi</option>
					<option value="Malaysia">Malaysia</option>
					<option value="Mali">Mali</option>
					<option value="Morocco">Morocco</option>
					<option value="Mozambique">Mozambique</option>
					<option value="Namibia">Namibia</option>
					<option value="Nepal">Nepal</option>
					<option value="Niger">Niger</option>
					<option value="Nigeria">Nigeria</option>
					<option value="Oman">Oman</option>
					<option value="Pakistan">Pakistan</option>
					<option value="Rwanda">Rwanda</option>
					<option value="Senegal">Senegal</option>
					<option value="Sierra Leone">Sierra Leone</option>
					<option value="South Africa">South Africa</option>
					<option value="Sri Lanka">Sri Lanka</option>
					<option value="Sudan">Sudan</option>
					<option value="Swaziland">Swaziland</option>
					<option value="Sweden">Sweden</option>
					<option value="Syrian Arab Republic">Syrian Arab Republic</option>
					<option value="Tajikistan">Tajikistan</option>
					<option value="Tanzania">Tanzania</option>
					<option value="Thailand">Thailand</option>
					<option value="Togo">Togo</option>
					<option value="Tunisia">Tunisia</option>
					<option value="Turkey">Turkey</option>
					<option value="Turkmenistan">Turkmenistan</option>
					<option value="Uganda">Uganda</option>
					<option value="Uzbekistan">Uzbekistan</option>
					<option value="VietNam">VietNam</option>
					<option value="Zambia">Zambia</option>
					<option value="Zimbabwe">Zimbabwe</option>
				</select> <br>
				<br>
				<div id="search-summary"></div>
			</div>
			<br>
		</div>
	</div>
	<div style="clear: both;"></div>
</div>

<script type="text/javascript">
	function filterUpdateMap() {

		document.getElementById("search-summary").innerHTML = 'Loading ...';

		// get the form values
		var center = $('#center').val();
		var year = $('#year').val();
		var country = $('#country').val();

		$.ajax({
			type : "POST",
			url : "ajax/map-country-proj-distrib",
			data : "program=" + center + "&year=" + year + "&country="
					+ country,
			success : function(response) {
				constructOpacityMapFromList(response);
				redraw();
				document.getElementById("search-summary").innerHTML = "";
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}
</script>

<!-- <div id="output-canvas"></div> -->
