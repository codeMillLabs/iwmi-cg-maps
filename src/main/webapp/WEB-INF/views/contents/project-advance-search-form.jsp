<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="container">
	<!-- <div id="view-menu">
		<a href="advn-project-form?navi=2">Search Projects</a>
		/
		<a href="advn-outcome-form?navi=2">Search Outputs</a>
	</div> -->
	<div id="search-form">
			<form:form action="advn-project-form" method="post"
				modelAttribute="advSerFrmModel">
				<table>
					<tr>
						<td><form:label path="startDate">Refine by year</form:label></td>
						<td><form:select path="startDate">
								<form:option value="">All</form:option>
								<form:options items="${projectYrs}" />
							</form:select></td>
					</tr>
					<tr><td colspan="2"><br/></td></tr>
					<tr>
						<td><form:label path="programs">Refine by Research Program</form:label></td>
						<td><form:select path="programs">
								<form:option value="">All</form:option>
								<form:options items="${programItmes}" />
							</form:select></td>
					</tr>
					<tr><td colspan="2"><br/></td></tr>
					<tr>
						<td><form:label path="regions">Regions</form:label></td>
						<td><form:select path="regions" onchange="regionSelected()">
								<form:option value="">All</form:option>
								<form:options items="${regionItems}" />
							</form:select></td>
					</tr>
					<tr><td colspan="2"><br/></td></tr>
					<tr>
						<td><form:label path="countries">Countries</form:label></td>
						<td><form:select path="countries">
								<form:option value="">All</form:option>
								<form:options items="${countryItems}" />
							</form:select></td>
					</tr>
					<tr><td colspan="2"><br/></td></tr>
					<tr>
						<td><form:label path="donors">Donors</form:label></td>
						<td><form:select path="donors">
								<form:option value="">All</form:option>
								<form:options items="${donorItems}" />
							</form:select></td>
					</tr>
					<tr><td colspan="2"><br/></td></tr>
					<tr>
						<td><form:label path="textSearch">Text Search</form:label></td>
						<td><form:input path="textSearch" /></td>
					</tr>
					<tr><td colspan="2"><br/></td></tr>
					<tr>
						<td colspan="2"><div id="advanced-search-buttons" align="center"><input class="search-button" type="submit" value="" />
							&nbsp;&nbsp; <input class="clear-button" type="reset" value="" /></div></td>
					</tr>
				</table>
			</form:form>
	</div>
</div>

<script type="text/javascript">
	function regionSelected() {
		
		// get the form values
		var regions = $('#regions').val();
		$.ajax({
			type : "POST",
			url : "ajax/regionsSel",
			data : "regions=" + regions,
			success : function(response) {
				var responseArray = response.split(",");
				var options = "";
				for (var i = 0; i < responseArray.length; i++) {
					options += '<option value="' + responseArray[i] + '">' + responseArray[i] + '</option>';
				}
				
				$('#countries')
			    .find('option')
			    .remove()
			    .end()
			    .html(options)
			;


			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}
</script>
