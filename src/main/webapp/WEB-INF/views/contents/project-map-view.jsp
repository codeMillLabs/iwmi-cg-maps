<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type="text/javascript"
	src="https://maps.google.com/maps/api/js?sensor=false"></script>

<script type="text/javascript">
	var colors = [ '#FF0000', '#00FF00', '#0000FF', '#FFFF00' ];
	var map;
	var dict = {};
	var dataCache;

	var opacityMap = {};

	function initialize() {
		constructOpacityMap();
		var myOptions = {
			center : new google.maps.LatLng(0, 0),
			zoom : 2,
			minZoom : 2,
			maxZoom : 4,
			mapTypeControl : false,
			navigationControl : true,
			scaleControl : true,
			scrollwheel : false,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById('map-canvas'),
				myOptions);

		// Initialize JSONP request
		var script = document.createElement('script');
		var url = [ 'https://www.googleapis.com/fusiontables/v1/query?' ];
		url.push('sql=');
		var query = 'SELECT name, kml_4326 FROM '
				+ '1foc3xO9DyfSIF6ofvN0kp2bxSfSeKog5FbdWdQ';
		var encodedQuery = encodeURIComponent(query);
		url.push(encodedQuery);
		url.push('&callback=drawMap');
		url.push('&key=AIzaSyAm9yWCV7JPCTHCJut8whOjARd7pwROFDQ');
		script.src = url.join('');
		var body = document.getElementsByTagName('body')[0];
		body.appendChild(script);
	}

	function redraw() {
		var myOptions = {
			center : new google.maps.LatLng(0, 0),
			zoom : 2,
			minZoom : 2,
			maxZoom : 4,
			mapTypeControl : false,
			navigationControl : true,
			scaleControl : true,
			scrollwheel : false,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById('map-canvas'),
				myOptions);
		drawPolygons(dataCache);
	}

	function drawMap(data) {
		dataCache = data;
		filterUpdateMap();
	}

	function drawPolygons(data) {
		dataCache = data;
		var rows = data['rows'];
		/* var resultString; */
		for ( var i in rows) {
			var countryName = rows[i][0];
			/* resultString = resultString + countryName+ ","; */
			if (countryName != 'Antarctica') {
				var newCoordinates = [];
				var geometries = rows[i][1]['geometries'];
				if (geometries) {
					for ( var j in geometries) {
						newCoordinates
								.push(constructNewCoordinates(geometries[j]));
					}
				} else {
					newCoordinates = constructNewCoordinates(rows[i][1]['geometry']);
				}
				var country = new google.maps.Polygon({
					paths : newCoordinates,
					strokeColor : '#ff9900',
					strokeOpacity : 0,
					strokeWeight : 1,
					fillColor : '#ff9900',
					fillOpacity : 1
				});

				var hashValue = generateHashValue(country.getPath().getAt(0)
						.lat(), country.getPath().getAt(0).lng(), country
						.getPath().getAt(1).lat(), country.getPath().getAt(1)
						.lng());

				dict[hashValue] = countryName;
				
				country.setOptions({
					fillColor : '#ff9900',
					fillOpacity : getOpacityValue(countryName)
				});

				google.maps.event.addListener(country, 'mouseover', function() {
					var hashValue = generateHashValue(this.getPath().getAt(0)
							.lat(), this.getPath().getAt(0).lng(), this
							.getPath().getAt(1).lat(), this.getPath().getAt(1)
							.lng());
					if (getOpacityValue(dict[hashValue]) > 0) {
						this.setOptions({
							fillColor : '#00ff00',
							fillOpacity : 1
						});
					}
				});

				google.maps.event.addListener(country, 'mouseout', function() {
					var hashValue = generateHashValue(this.getPath().getAt(0)
							.lat(), this.getPath().getAt(0).lng(), this
							.getPath().getAt(1).lat(), this.getPath().getAt(1)
							.lng());
					this.setOptions({
						fillColor : '#ff9900',
						fillOpacity : getOpacityValue(dict[hashValue])
					});
				});
				google.maps.event
						.addListener(
								country,
								'click',
								function(event) {
									var hashValue = generateHashValue(this
											.getPath().getAt(0).lat(), this
											.getPath().getAt(0).lng(), this
											.getPath().getAt(1).lat(), this
											.getPath().getAt(1).lng());
									var country = dict[hashValue];
									if (getOpacityValue(dict[hashValue]) > 0) {
									var center = $('#program').val();
									var year = $('#year').val();
									country = checkCountryName(country);
									var url = "ajax/map-country-country-details?program="
											+ encodeURIComponent(center)
											+ "&year="
											+ year
											+ "&country="
											+ encodeURIComponent(country);
									$.colorbox({
												href : url
											});
									}
								});

				country.setMap(map);
			}
		}
		/* var resultArray = resultString.split(",");
		resultArray.sort();
		resultString = "";
		for (var i=0;i<resultArray.length;i++)
		{
			resultString = resultString + "&lt;option value=&quot;"+resultArray[i]+"&quot;&gt;"+resultArray[i]+"&lt;/option&gt;<br/>";
		}
		document.getElementById("output-canvas").innerHTML = resultString; */
	}

	function constructNewCoordinates(polygon) {
		var newCoordinates = [];
		var coordinates = polygon['coordinates'][0];
		for ( var i in coordinates) {
			newCoordinates.push(new google.maps.LatLng(coordinates[i][1],
					coordinates[i][0]));
		}
		return newCoordinates;
	}

	function generateHashValue(lat1, lng1, lat2, lng2) {
		return lat1 + "|" + lng1 + "|" + lat2 + "|" + lng2;
	}

	function constructOpacityMapFromList(list) {
		constructOpacityMap();
		var splittedArray = list.split("|");

		for ( var i = 0; i < splittedArray.length; i++) {
			var splittedValue = splittedArray[i].split("_");
			opacityMap[splittedValue[0]] = splittedValue[1];
		}

		return opacityMap;
	}

	function constructOpacityMap() {
		opacityMap['Afghanistan'] = 0;
		opacityMap['United Arab Emirates'] = 0;
		opacityMap['Burkina Faso'] = 0;
		opacityMap['Bangladesh'] = 0;
		opacityMap['Congo (Kinshasa)'] = 0;
		opacityMap['Cuba'] = 0;
		opacityMap['Czech Rep.'] = 0;
		opacityMap['Denmark'] = 0;
		opacityMap['Ethiopia'] = 0;
		opacityMap['Guinea Bissau'] = 0;
		opacityMap['Eq. Guinea'] = 0;
		opacityMap['Croatia'] = 0;
		opacityMap['Indonesia'] = 0;
		opacityMap['Iran'] = 0;
		opacityMap['Jordan'] = 0;
		opacityMap['Kazakhstan'] = 0;
		opacityMap['Cambodia'] = 0;
		opacityMap['Laos'] = 0;
		opacityMap['New Zealand'] = 0;
		opacityMap['Papua New Guinea'] = 0;
		opacityMap['Russia'] = 0.0;
		opacityMap['Rwanda'] = 0;
		opacityMap['Somalia'] = 0;
		opacityMap['Serbia'] = 0;
		opacityMap['Trinidad and Tobago'] = 0;
		opacityMap['Tunisia'] = 0;
		opacityMap['Yemen'] = 0;
		opacityMap['Zimbabwe'] = 0;
		opacityMap['Angola'] = 0;
		opacityMap['Armenia'] = 0;
		opacityMap['Fr. S. and Antarctic Lands'] = 0;
		opacityMap['Burundi'] = 0;
		opacityMap['Bosnia and Herz.'] = 0;
		opacityMap['Colombia'] = 0;
		opacityMap['Cyprus'] = 0;
		opacityMap['Ecuador'] = 0;
		opacityMap['Eritrea'] = 0;
		opacityMap['Honduras'] = 0;
		opacityMap['Kuwait'] = 0;
		opacityMap['Sri Lanka'] = 0;
		opacityMap['Lithuania'] = 0;
		opacityMap['Moldova'] = 0;
		opacityMap['Nicaragua'] = 0;
		opacityMap['Nepal'] = 0;
		opacityMap['Panama'] = 0;
		opacityMap['Portugal'] = 0;
		opacityMap['Qatar'] = 0;
		opacityMap['Romania'] = 0;
		opacityMap['Solomon Is.'] = 0;
		opacityMap['Slovenia'] = 0;
		opacityMap['Vietnam'] = 0;
		opacityMap['Albania'] = 0;
		opacityMap['Australia'] = 0;
		opacityMap['Azerbaijan'] = 0;
		opacityMap['Belgium'] = 0;
		opacityMap['Switzerland'] = 0;
		opacityMap['Congo (Brazzaville)'] = 0;
		opacityMap['Costa Rica'] = 0;
		opacityMap['N. Cyprus'] = 0;
		opacityMap['France'] = 0;
		opacityMap['Guyana'] = 0;
		opacityMap['Luxembourg'] = 0;
		opacityMap['Latvia'] = 0;
		opacityMap['Madagascar'] = 0;
		opacityMap['Mali'] = 0;
		opacityMap['Mozambique'] = 0;
		opacityMap['Saudi Arabia'] = 0;
		opacityMap['Slovakia'] = 0;
		opacityMap['Syria'] = 0;
		opacityMap['Thailand'] = 0;
		opacityMap['Uzbekistan'] = 0;
		opacityMap['South Africa'] = 0;
		opacityMap['Argentina'] = 0;
		opacityMap['Brunei'] = 0;
		opacityMap['Botswana'] = 0;
		opacityMap['Canada'] = 0;
		opacityMap['China'] = 0;
		opacityMap['Ivory Coast'] = 0;
		opacityMap['Germany'] = 0;
		opacityMap['Egypt'] = 0;
		opacityMap['Spain'] = 0;
		opacityMap['Finland'] = 0;
		opacityMap['Gambia'] = 0;
		opacityMap['Greenland'] = 0;
		opacityMap['Iraq'] = 0;
		opacityMap['S. Korea'] = 0;
		opacityMap['Kosovo'] = 0;
		opacityMap['Malaysia'] = 0;
		opacityMap['Namibia'] = 0;
		opacityMap['Norway'] = 0;
		opacityMap['N. Korea'] = 0;
		opacityMap['Somaliland'] = 0;
		opacityMap['Turkmenistan'] = 0;
		opacityMap['Taiwan'] = 0;
		opacityMap['Tanzania'] = 0;
		opacityMap['Venezuela'] = 0;
		opacityMap['Vanuatu'] = 0;
		opacityMap['Antarctica'] = 0;
		opacityMap['Benin'] = 0;
		opacityMap['Belarus'] = 0;
		opacityMap['Belize'] = 0;
		opacityMap['Bolivia'] = 0;
		opacityMap['Brazil'] = 0;
		opacityMap['Bhutan'] = 0;
		opacityMap['Central African Rep.'] = 0;
		opacityMap['Chile'] = 0;
		opacityMap['Fiji'] = 0;
		opacityMap['Georgia'] = 0;
		opacityMap['Hungary'] = 0;
		opacityMap['Ireland'] = 0;
		opacityMap['Italy'] = 0;
		opacityMap['Jamaica'] = 0;
		opacityMap['Japan'] = 0;
		opacityMap['Kyrgyzstan'] = 0;
		opacityMap['Morocco'] = 0;
		opacityMap['Macedonia'] = 0;
		opacityMap['Malawi'] = 0;
		opacityMap['Suriname'] = 0;
		opacityMap['Sweden'] = 0;
		opacityMap['East Timor'] = 0;
		opacityMap['Turkey'] = 0;
		opacityMap['United States'] = 0;
		opacityMap['Austria'] = 0;
		opacityMap['Cameroon'] = 0;
		opacityMap['Djibouti'] = 0;
		opacityMap['Dominican Rep.'] = 0;
		opacityMap['Falkland Is.'] = 0;
		opacityMap['Gabon'] = 0;
		opacityMap['United Kingdom'] = 0;
		opacityMap['Ghana'] = 0;
		opacityMap['Greece'] = 0;
		opacityMap['Iceland'] = 0;
		opacityMap['Lebanon'] = 0;
		opacityMap['Libya'] = 0;
		opacityMap['Lesotho'] = 0;
		opacityMap['Mexico'] = 0;
		opacityMap['Myanmar'] = 0;
		opacityMap['Montenegro'] = 0;
		opacityMap['Mongolia'] = 0;
		opacityMap['Niger'] = 0;
		opacityMap['Nigeria'] = 0;
		opacityMap['Netherlands'] = 0;
		opacityMap['Sudan'] = 0;
		opacityMap['S. Sudan'] = 0;
		opacityMap['Senegal'] = 0;
		opacityMap['Sierra Leone'] = 0;
		opacityMap['El Salvador'] = 0;
		opacityMap['Chad'] = 0;
		opacityMap['Tajikistan'] = 0;
		opacityMap['Uganda'] = 0;
		opacityMap['Ukraine'] = 0;
		opacityMap['Uruguay'] = 0;
		opacityMap['Zambia'] = 0;
		opacityMap['Bulgaria'] = 0;
		opacityMap['Algeria'] = 0;
		opacityMap['Estonia'] = 0;
		opacityMap['Haiti'] = 0;
		opacityMap['India'] = 0;
		opacityMap['Kenya'] = 0;
		opacityMap['Liberia'] = 0;
		opacityMap['Mauritania'] = 0;
		opacityMap['New Caledonia'] = 0;
		opacityMap['Pakistan'] = 0;
		opacityMap['Peru'] = 0;
		opacityMap['Philippines'] = 0;
		opacityMap['Paraguay'] = 0;
		opacityMap['Swaziland'] = 0;
		opacityMap['West Bank'] = 0;
		opacityMap['Bahamas'] = 0;
		opacityMap['Guinea'] = 0;
		opacityMap['Guatemala'] = 0;
		opacityMap['Israel'] = 0;
		opacityMap['Oman'] = 0;
		opacityMap['Poland'] = 0;
		opacityMap['Puerto Rico'] = 0;
		opacityMap['W. Sahara'] = 0;
		opacityMap['Togo'] = 0;
	}
	
	function checkCountryName(countryName)
	{
		if(countryName == "Kyrgyzstan")
        {
            return  "Kyrgyz Republic";
        }
        else if(countryName == "Syria")
        {
            return  "Syrian Arab Republic";
        }
        else if(countryName  == "Andorra")
        {
            return  "Andorra";
        }
        else if(countryName == "Vietnam")
        {
            return  "VietNam";
        }
        else if(countryName == "Brunei")
        {
            return  "Brunei Darussalam";
        }
        else if(countryName == "Antarctica")
        {
            return  "Antarctica (the territory South of 60 deg S)";
        }
        else if(countryName == "Anguilla")
        {
            return  "Anguilla";
        }
        else if(countryName == "Congo (Kinshasa)")
        {
            return  "Congo";
        }
        else if(countryName == "Laos")
        {
            return  "Lao People's Democratic Republic";
        }
		
		return countryName;
	}
	
	
	function getOpacityValue(countryName) {
		if (typeof opacityMap[countryName] === "undefined") {
			return 0;
		} else {
			return opacityMap[countryName];
		}
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-container">
	<div class="view-menu">
		<a href="map-view">Map View</a>
		|
		<a href="list-view">List View</a>
	</div>
	<div id="map-canvas"></div>
	<div id="right-side-panel">
		<form:form action="list-view" method="post"
			modelAttribute="projListModel">
			<div>
				<p></p>
				Refine by Research Program<br>
				<form:select onchange="filterUpdateMap();" path="program">
					<form:option value="">All</form:option>
					<form:options items="${programList}" />
				</form:select>
			</div>
			<div>
				<br>Refine by year<br>
				<form:select onchange="filterUpdateMap();" path="year">
					<form:option value="">All</form:option>
					<form:options items="${projectYrs}" />
				</form:select>
				<br>
			</div>
			<div>
				<br>Refine by country<br>
				<form:select onchange="filterUpdateMap();" path="country">
					<form:option value="">All</form:option>
					<form:options items="${countryList}" />
				</form:select>
				<br>
				<br>
				<div id="search-summary"></div>
			</div>
		</form:form>
	</div>
	<div style="clear: both;"></div>
</div>

<script type="text/javascript">
	function filterUpdateMap() {

		document.getElementById("search-summary").innerHTML = 'Loading ...';

		// get the form values
		var center = $('#program').val();
		var year = $('#year').val();
		var country = $('#country').val();

		$.ajax({
			type : "POST",
			url : "ajax/map-country-proj-distrib",
			data : "program=" + center + "&year=" + year + "&country="
					+ country,
			success : function(response) {
				constructOpacityMapFromList(response);
				redraw();
				document.getElementById("search-summary").innerHTML = "";
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}
</script>

<!-- <div id="output-canvas"></div> -->
