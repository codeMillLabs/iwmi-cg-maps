<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div id="result-layout">
		<c:if test="${not empty  isNoData}">
			<form action="advn-outcome-data" method="GET">
				<div class="search-output-filter">
					Program <select name="prog" onchange="this.form.submit()">
						<c:forEach var="program" items="${programs}">
							<option value="${program}">${program}</option>
						</c:forEach>
					</select>
				</div>
			</form>
		</c:if>
		<div class="search-result-layout">
			<div>
				<div class="result-header">
					<c:if test="${not empty isNoData}">
						<div>
							Total No of Projects found:
							<c:out value="${dataSize}" />
						</div>
					</c:if>
				</div>
				<c:choose>
					<c:when test="${not empty isNoData}">
						<c:forEach var="entry" items="${dataMap}">
							<c:if test="${not empty entry.value}">
								<c:forEach var="prj" items="${entry.value}">
									<div class="project-name-link">
										<a href="./prj-details?prjId=${prj.subProjectId}">
											${prj.projectName} </a>
									</div>
									<div class="result-detail-content-row">
										<div class="result-detail-18">Output Name :</div>
										<div class="result-detail-80">${prj.name}</div>
									</div>
									<div class="clear"></div>
									<br />
								</c:forEach>
							</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<div class="project-name-link">No data found for search.</div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>

</div>

