<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amila@hashcodesys.com
 * @version 1.0
--%>
<div id="footer-layout">
	<div id="footer">
		<div id="footer-top">
			<div class="container"><div id="footer-message">International Water Management Institute, 127, Sunil Mawatha, Pelawatte, Battaramulla, Sri Lanka. </div></div>
		</div>
		<div id="footer-bottom">
			<div class="container">
				<div id="footer-links">
				<ul>
					<li class="first"><a href="map-view" >Home</a></li>
					<li><a href="advn-project-form?navi=3">Advanced Search</a></li>
				</ul>
				</div>
				<div id="powered-by">
				powered by
				<a target="_blank" href="http://www.hashcodesys.com/">hashCode</a>
				</div>
			</div>
		</div>
	</div>
</div>