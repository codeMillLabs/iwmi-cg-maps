<%-- 
 * hashCode Solutions.
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
--%>

<div id="header-layout">
	<div id="header">
		<!-- start header-->
		<div id="top_menu">
			<!-- start top menu -->
			<div class="container">
				<div id="main_menu">
					<ul>
						<%
							String navi = request.getParameter("navi");
							Object naviAtt = request.getAttribute("naviAtt");
							if(null == navi && null != naviAtt)
							{
								navi = String.valueOf(naviAtt);
							}
						
							if (navi == null || navi.equals("1")) {
						%>
						<li class="main_menu_active"><a href="map-view">Home</a></li>
						<%
							} else {
						%>
						<li><a href="map-view">Home</a></li>
						<%-- <%
							}
							if (null != navi && navi.equals("2")) {
						%>
						<li class="main_menu_active"><a
							href="ongoing-map-view?navi=2">Ongoing Research</a></li>
						<%
							} else {
						%>
						<li><a href="ongoing-map-view?navi=2">Ongoing Research</a></li> --%>
						<%
							}
							if (null != navi && navi.equals("3")) {
						%>
						<li class="main_menu_active"><a
							href="advn-project-form?navi=3">Advanced Search</a></li>
						<%
							} else {
						%>
						<li><a href="advn-project-form?navi=3">Advanced Search</a></li>
						<%
							}
							//if (null != navi && navi.equals("4")) {
						%>
						<%-- <li class="main_menu_active"><a href="#">Help</a></li>
						<%
							} else {
						%>
						<li><a href="#">Help</a></li>
						<%
							}
						%> --%>
					</ul>
				</div>
				<!-- <div id="login_menu">
					<ul>
						<li><a>login</a></li>
					</ul>
				</div> -->
				<div class="clear"></div>
			</div>
		</div>
		<!-- end top menu -->
		<div id="logo_bar">
			<!-- start logo bar-->
			<div class="container">
				<div id="logo"></div>
				<div id="logo2"></div>
				<div id="project-title">
					<%
						if (null == navi || navi.equals("1")) {
					%>
					<h1>IWMI Research Projects<br/>(All Projects)</h1>
					<%
						} else if (null != navi && navi.equals("2")) {
					%>
					<h1>IWMI Research Projects<br/>(Ongoing Projects)</h1>
					<%
						} else {
					%>
					<h1>IWMI Research Projects</h1>
					<%
						}
					%>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<!-- end logo bar-->
	</div>
	<!-- end header-->
</div>