/*
 * FILENAME
 *     Utilities.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.util;

/**
 * <p>
 * Utilities class for
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 */
public final class Utilities
{

    public static int LIST_VIEW_PAGE_SIZE = 20;
    public static int ADV_VIEW_PAGE_SIZE = 20;

    public static boolean isNullOrEmpty(final String value)
    {
        return (value == null) || (value.isEmpty());
    }

    /**
     * <p>
     * Country Name fix method. This is used to fix difference between country names of Google Map and IWMI database.
     * </p>
     * 
     * @param countryName
     *            countryName
     * 
     * @return fixed country name
     */
    public static String countryNameFix(final String countryName)
    {
        if (countryName.equals("Kyrgyz Republic"))
        {
            return "Kyrgyzstan";
        }
        else if (countryName.equals("Syrian Arab Republic"))
        {
            return "Syria";
        }
        else if (countryName.equals("Andorra"))
        {
            return "Andorra";
        }
        else if (countryName.equals("VietNam"))
        {
            return "Vietnam";
        }
        else if (countryName.equals("Brunei Darussalam"))
        {
            return "Brunei";
        }
        else if (countryName.equals("Antarctica (the territory South of 60 deg S)"))
        {
            return "Antarctica";
        }
        else if (countryName.equals("Anguilla"))
        {
            return "Anguilla";
        }
        else if (countryName.equals("Congo"))
        {
            return "Congo (Kinshasa)";
        }
        else if (countryName.equals("Lao People's Democratic Republic"))
        {
            return "Laos";
        }

        return countryName;
    }

    public static String getToYear(String yearStr)
    {
        if (isNullOrEmpty(yearStr))
            return null;
        else
        {
            int year = Integer.parseInt(yearStr);

            return "" + (year + 1);
        }
    }
}
