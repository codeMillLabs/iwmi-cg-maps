/*
 * FILENAME
 *     ProjectDataViewRowMapper.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.hashcode.iwmi.cgmaps.model.ProjectInfoView;

/**
 * <p>
 * Project Data View row mapping according to the data from data source.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 * 
 */
public class ProjectDataViewRowMapper implements RowMapper<ProjectInfoView> {

	private static final Logger logger = LoggerFactory
			.getLogger(ProjectDataViewRowMapper.class);

	protected static final String PROJ_ID = "SubProjectId";
	protected static final String PROJ_SHORT_NAME = "ProjectShortName";
	protected static final String PROJ_LONG_NAME = "ProjectLongName";
	protected static final String PROJ_CODE = "ProjectCode";
	protected static final String DESC = "Description";
	protected static final String START_DATE = "StartDate";
	protected static final String END_DATE = "EndDate";
	protected static final String EXT_DATE = "ExtendedDate";
	protected static final String DONORS = "Donors";
	protected static final String LOCATIONS = "Locations";
	protected static final String THEME = "Theme";
	protected static final String PROGRAM = "Program";
	protected static final String PROJ_LEADER = "projectLeader";
	protected static final String PL_MAIL = "PLemail";
	protected static final String WEB_TITLE = "WebTitle";
	protected static final String WEB_ADDRESS = "WebAddress";
	protected static final String CONTACT_NAME = "ContactName";
	protected static final String CONTACT_EMAIL = "ContactEmail";
	protected static final String PROJ_STATUS = "ProjectStatus";

	protected static final String CNTRY_REGION = "geo_region_name";
	protected static final String COUNTRY_PREFIX = "c.";

	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(
			"yyyy-MM-dd");

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */
	public ProjectInfoView mapRow(ResultSet rs, int rowNo) throws SQLException {

		logger.debug("Going to Map the row data to object");

		ProjectInfoView view = new ProjectInfoView();
		view.setProjectId(rs.getLong(PROJ_ID));
		view.setShortName(rs.getString(PROJ_SHORT_NAME));
		view.setLongName(rs.getString(PROJ_LONG_NAME));
		view.setCode(rs.getString(PROJ_CODE));
		view.setDescription(rs.getString(DESC));
		view.setStartDate(rs.getDate(START_DATE));
		view.setEndDate(rs.getDate(END_DATE));
		view.setExtendedDate(rs.getDate(EXT_DATE));
		view.setDonors(getValuesAsList(rs.getString(DONORS)));
		view.setDonorsStr(rs.getString(DONORS));
		view.setLocations(getValuesAsList(rs.getString(LOCATIONS)));
		view.setLocationStr(rs.getString(LOCATIONS));
		view.setTheme(rs.getString(THEME));
		view.setRegion(rs.getString(CNTRY_REGION));
		view.setProgram(rs.getString(PROGRAM));
		view.setProjectLeader(rs.getString(PROJ_LEADER));
		view.setPlEmail(rs.getString(PL_MAIL));
		view.setWebTitle(rs.getString(WEB_TITLE));
		view.setWebAddress(rs.getString(WEB_ADDRESS));
		view.setContactName(rs.getString(CONTACT_NAME));
		view.setContactEmail(rs.getString(CONTACT_EMAIL));
		view.setProjectStatus(rs.getString(PROJ_STATUS));

		logger.debug("Data mapping successfully done {}.", view);
		return view;
	}

	private List<String> getValuesAsList(final String dbValues) {
		List<String> values = new ArrayList<String>();

		if (dbValues != null && !dbValues.isEmpty()) {
			String[] splits = dbValues.split(",", -1);
			for (String val : splits) {
				values.add(val);
			}
		}

		return values;
	}

	private Date getDateFromString(String dateStr) {
		try {
			return DATE_FORMATTER.parse(dateStr);
		} catch (Exception e) {
			logger.error("Error encountered in converting the String date to Date"
					+ e.getMessage());
			return Calendar.getInstance().getTime();
		}
	}

}
