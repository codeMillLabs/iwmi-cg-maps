/*
 * FILENAME
 *     ProjectInfoRowMapper.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.services;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.hashcode.iwmi.cgmaps.model.ProjectListView;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * 
 * <p>
 * Custom implementation for the Project info row mapping.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version $Id$
 * 
 */
public class ProjectListViewRowMapper implements RowMapper<ProjectListView>
{
    private static final Logger logger = LoggerFactory.getLogger(ProjectListViewRowMapper.class);

    public static final String PROJ_ID = "SubProjectId";
    public static final String THEME = "Theme";
    public static final String PROGRAM = "Program";
    public static final String WEB_TITLE = "WebTitle";
    public static final String PROJECT_IMAGE = "ProjectImage";
    public static final String PROJECT_STATE = "ProjectStatus";
    
    public static final String COUNTRY_NAME = "country_name";

    public ProjectListView mapRow(ResultSet rs, int index) throws SQLException
    {
        logger.debug("Project Info row mapping.");
        ProjectListView view = new ProjectListView();
        view.setProjectId(rs.getString(PROJ_ID));
        view.setProgram(rs.getString(PROGRAM));
        view.setTheme(rs.getString(THEME));
        view.setWebTitle(rs.getString(WEB_TITLE));
        view.setProjectStatus(rs.getString(PROJECT_STATE));
        view.setProjectImage(rs.getString(PROJECT_IMAGE));
        return view;
    }

}
