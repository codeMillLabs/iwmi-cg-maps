/*
 * FILENAME
 *     AdvanceSearchService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.services;

import java.util.List;

import com.hashcode.iwmi.cgmaps.model.ProjectInfoView;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Advance Search service for the CG Maps.
 * 
 * This will provide the fully detailed search.
 * </p>
 *
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 **/
public interface AdvanceSearchService
{
    /**
     * <p>
     * Search projects according to the given criteria.
     * </p>
     *
     * @param criteria advance search criteria
     * @return list of project info according to the criteria.
     */
    List<ProjectInfoView> searchProjects(AdvancedSearchCriteria criteria);

}
