/*
 * FILENAME
 *     AdvancedSearchCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.services;

import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.CNTRY_REGION;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.DESC;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.DONORS;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.END_DATE;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.EXT_DATE;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.PROGRAM;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.PROJ_LONG_NAME;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.PROJ_SHORT_NAME;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.START_DATE;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.THEME;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.WEB_TITLE;
import static com.hashcode.iwmi.cgmaps.services.ProjectListViewRowMapper.COUNTRY_NAME;
import static com.hashcode.iwmi.cgmaps.services.ProjectOutcomeRowMapper.PO_COMMENTS;
import static com.hashcode.iwmi.cgmaps.services.ProjectOutcomeRowMapper.SUB_PROJ_NAME;
import static com.hashcode.iwmi.cgmaps.util.Utilities.isNullOrEmpty;
import static java.lang.Integer.valueOf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Advanced search based criteria builder.
 * 
 * This will build the criteria according to the advance search requirements.
 * </p>
 * 
 * @author Amila Silva
 * @contact amila@hashcodesys.com
 * @version 1.0
 * 
 */
public class AdvancedSearchCriteria extends SearchCriteria
{

    private static final long serialVersionUID = 3408097292841899772L;
    private static final Logger logger = LoggerFactory.getLogger(AdvancedSearchCriteria.class);

    private SearchCriteriaType searchCriteriaType;
    private String startDate;
    private List<String> programs = new ArrayList<String>();
    private List<String> regions = new ArrayList<String>();
    private List<String> countries = new ArrayList<String>();
    private List<String> donors = new ArrayList<String>();
    private String textSearch;

    /**
     * <p>
     * Getter for startDate.
     * </p>
     * 
     * @return the startDate
     */
    public String getStartDate()
    {
        return startDate;
    }

    /**
     * <p>
     * Setting value for startDate.
     * </p>
     * 
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    /**
     * <p>
     * Getter for programs.
     * </p>
     * 
     * @return the programs
     */
    public List<String> getPrograms()
    {
        if (programs == null)
        {
            programs = new ArrayList<String>();
        }
        return programs;
    }

    /**
     * <p>
     * Setting value for programs.
     * </p>
     * 
     * @param programs
     *            the programs to set
     */
    public void setPrograms(List<String> programs)
    {
        this.programs = programs;
    }

    /**
     * <p>
     * Getter for regions.
     * </p>
     * 
     * @return the regions
     */
    public List<String> getRegions()
    {
        if (regions == null)
        {
            regions = new ArrayList<String>();
        }
        return regions;
    }

    /**
     * <p>
     * Setting value for regions.
     * </p>
     * 
     * @param regions
     *            the regions to set
     */
    public void setRegions(List<String> regions)
    {
        this.regions = regions;
    }

    /**
     * <p>
     * Getter for countries.
     * </p>
     * 
     * @return the countries
     */
    public List<String> getCountries()
    {
        if (countries == null)
        {
            countries = new ArrayList<String>();
        }
        return countries;
    }

    /**
     * <p>
     * Setting value for countries.
     * </p>
     * 
     * @param countries
     *            the countries to set
     */
    public void setCountries(List<String> countries)
    {
        this.countries = countries;
    }

    /**
     * <p>
     * Getter for donors.
     * </p>
     * 
     * @return the donors
     */
    public List<String> getDonors()
    {
        if (donors == null)
        {
            donors = new ArrayList<String>();
        }
        return donors;
    }

    /**
     * <p>
     * Setting value for donors.
     * </p>
     * 
     * @param donors
     *            the donors to set
     */
    public void setDonors(List<String> donors)
    {
        this.donors = donors;
    }

    /**
     * <p>
     * Getter for textSearch.
     * </p>
     * 
     * @return the textSearch
     */
    public String getTextSearch()
    {
        return textSearch;
    }

    /**
     * <p>
     * Setting value for textSearch.
     * </p>
     * 
     * @param textSearch
     *            the textSearch to set
     */
    public void setTextSearch(String textSearch)
    {
        this.textSearch = textSearch;
    }

    /**
     * <p>
     * Check for programs existence in criteria
     * </p>
     * 
     * @return boolean
     */
    public boolean hasPrograms()
    {
        return !programs.isEmpty();
    }

    /**
     * <p>
     * Check for project start date existence in criteria
     * </p>
     * 
     * @return boolean
     */
    public boolean hasStartDate()
    {
        return !isNullOrEmpty(startDate);
    }

    @Override
    public boolean hasCriteria()
    {
        return startDate != null || !programs.isEmpty() || !regions.isEmpty() || !countries.isEmpty()
            || !donors.isEmpty() || textSearch != null;
    }

    public String getCriteriaQuery(Map<String, String> allProgramsYearMap)
    {

        switch (searchCriteriaType)
        {
            case PROJECT_SEARCH:
                return createProjectSearchCriteria(allProgramsYearMap);

            case OUTCOME_SEARCH:
                return createProjectOurcomeCriteria();
        }

        return "";
    }

    private String createProjectOurcomeCriteria()
    {
        StringBuilder query = new StringBuilder();
        boolean isAdded = false;

        if (!isNullOrEmpty(startDate))
        {
            query.append(" (( pd." + EXT_DATE + " IS NULL AND ").append(startDate)
                .append(" BETWEEN  pd." + START_DATE + " AND pd." + END_DATE + " ) ");
            query.append(" OR ( ").append(startDate)
                .append(" BETWEEN pd." + START_DATE + " AND pd." + EXT_DATE + " )) ");
            isAdded = true;
        }

        if (!programs.isEmpty())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFields(" pd." + PROGRAM, programs);
            query.append(subQuery);
            isAdded = true;
        }

        if (!regions.isEmpty())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFields(" c." + CNTRY_REGION, regions);
            query.append(subQuery);
            isAdded = true;
        }

        if (!countries.isEmpty())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFields(" c." + COUNTRY_NAME, countries);
            query.append(subQuery);
            isAdded = true;
        }

        if (!donors.isEmpty())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFieldsLike(" pd." + DONORS, donors);
            query.append(subQuery);
            isAdded = true;
        }

        if (textSearch != null)
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String[] fields = new String[] {
                PO_COMMENTS, SUB_PROJ_NAME
            };
            String subQuery = getQueryForOutcomeTextSearch(fields, textSearch);
            query.append(subQuery);
        }
        logger.debug("Advance search query criteria :{}", query.toString());

        return query.toString();
    }

    private String createProjectSearchCriteria(Map<String, String> allProgramsYearMap)
    {
        StringBuilder query = new StringBuilder();
        boolean isAdded = false;

        if (hasPrograms() && !hasStartDate())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFieldsScenario1(" pd." + PROGRAM, programs, allProgramsYearMap);
            query.append(subQuery);
            isAdded = true;
        }
        else if (!hasPrograms() && hasStartDate())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFieldsScenario2(allProgramsYearMap);
            query.append(subQuery);
            isAdded = true;
        }
        else if (hasPrograms() && hasStartDate())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }

            String subQuery = getQueryForMultipleFieldsScenario3(" pd." + PROGRAM, programs, allProgramsYearMap);

            query.append(subQuery);
            isAdded = true;
        }
        else
        {

        }

        if (!regions.isEmpty())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFields(" c." + CNTRY_REGION, regions);
            query.append(subQuery);
            isAdded = true;
        }

        if (!countries.isEmpty())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFields(" c." + COUNTRY_NAME, countries);
            query.append(subQuery);
            isAdded = true;
        }

        if (!donors.isEmpty())
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String subQuery = getQueryForMultipleFieldsLike(" pd." + DONORS, donors);
            query.append(subQuery);
            isAdded = true;
        }

        if (textSearch != null)
        {
            if (isAdded)
            {
                query.append(" AND ");
            }
            String[] fields = new String[] {
                PROJ_SHORT_NAME, PROJ_LONG_NAME, DESC, THEME, WEB_TITLE, PROGRAM
            };
            String subQuery = getQueryForTextSearch(fields, textSearch);
            query.append(subQuery);
        }
        logger.debug("Advance search query criteria :{}", query.toString());
        return query.toString();
    }

    private String getQueryForTextSearch(String[] feilds, String value)
    {

        StringBuilder subQuery = new StringBuilder();

        int count = 1;
        subQuery.append(" ( ");
        for (String field : feilds)
        {
            subQuery.append(" pd." + field + " like " + " '%" + value + "%' ");

            if (count < feilds.length)
            {
                subQuery.append(" OR ");
            }
            count++;
        }
        subQuery.append(" ) ");
        return subQuery.toString();
    }

    private String getQueryForOutcomeTextSearch(String[] feilds, String value)
    {

        StringBuilder subQuery = new StringBuilder();

        int count = 1;
        subQuery.append(" ( ");
        for (String field : feilds)
        {
            subQuery.append(" p." + field + " like " + " '%" + value + "%' ");

            if (count < feilds.length)
            {
                subQuery.append(" OR ");
            }
            count++;
        }
        subQuery.append(" ) ");
        return subQuery.toString();
    }

    private String getQueryForMultipleFields(String fieldName, List<String> values)
    {
        StringBuilder subQuery = new StringBuilder();
        int count = 1;
        subQuery.append(" ( ");
        for (String value : values)
        {
            subQuery.append(fieldName + " = ").append(" '" + value + "' ");

            if (count < values.size())
            {
                subQuery.append(" OR ");
            }
            count++;
        }
        subQuery.append(" ) ");
        return subQuery.toString();
    }

    private String getQueryForMultipleFieldsLike(String fieldName, List<String> values)
    {
        StringBuilder subQuery = new StringBuilder();
        int count = 1;
        subQuery.append(" ( ");
        for (String value : values)
        {
            subQuery.append(fieldName + " like ").append(" '%" + value + "%' ");

            if (count < values.size())
            {
                subQuery.append(" OR ");
            }
            count++;
        }
        subQuery.append(" )");
        return subQuery.toString();
    }

    private String getQueryForMultipleFieldsScenario1(String fieldName, List<String> values,
        Map<String, String> allProgramsYearMap)
    {
        StringBuilder subQuery = new StringBuilder();

        subQuery.append(" ( ");
        Iterator<String> keyItr = programs.iterator();
        while (keyItr.hasNext())
        {
            String program = keyItr.next();
            subQuery.append(" ( " + fieldName + " = ").append(" '" + program + "' ");

            //			String yrStr = allProgramsYearMap.get(program);
            //			if (!isNullOrEmpty(yrStr)) {
            //				subQuery.append(getQueryPartForAfterFromYear(yrStr));
            //			}

            subQuery.append(" )");

            if (keyItr.hasNext())
            {
                subQuery.append(" OR ");
            }
        }
        subQuery.append(" ) ");
        return subQuery.toString();
    }

    private String getQueryForMultipleFieldsScenario2(Map<String, String> allProgramsYearMap)
    {
        StringBuilder subQuery = new StringBuilder();
        subQuery.append(" ( ");
        subQuery.append(getQueryPartForFromYear(startDate, false));
        subQuery.append(" ) ");
        return subQuery.toString();
    }

    private String getQueryForMultipleFieldsScenario3(String fieldName, List<String> programs,
        Map<String, String> allProgramsYearMap)
    {
        StringBuilder subQuery = new StringBuilder();
        subQuery.append(" ( ");

        Iterator<String> keyItr = programs.iterator();
        while (keyItr.hasNext())
        {
            String program = keyItr.next();

            subQuery.append(" ( " + fieldName + " = ").append(" '" + program + "' ");
            String yrStr = allProgramsYearMap.get(program);

            int filterYear = (!isNullOrEmpty(yrStr)) ? valueOf(yrStr) : 1900;
            int frmYear = valueOf(startDate);

            if (frmYear >= filterYear)
            {
                subQuery.append(getQueryPartForFromYear(startDate, true));
            }
            else
            {
                subQuery.append(getQueryPartForFromYear("4000", true));
            }
            subQuery.append(" ) ");

            if (keyItr.hasNext())
            {
                subQuery.append(" OR ");
            }
        }
        subQuery.append(" ) ");
        return subQuery.toString();
    }

    //	private String getQueryPartForAfterFromYear(String year) {
    //		StringBuilder queryPart = new StringBuilder();
    //		queryPart.append(" AND ");
    //		queryPart.append(" pd." + START_DATE + " >= '").append(year)
    //				.append("' ");
    //		return queryPart.toString();
    //	}

    private String getQueryPartForFromYear(String year, boolean needAnd)
    {
        StringBuilder queryPart = new StringBuilder();
        if (needAnd)
            queryPart.append(" AND ");

        queryPart.append(" (( pd." + EXT_DATE + " IS NULL AND ").append(year)
            .append(" BETWEEN  pd." + START_DATE + " AND pd." + END_DATE + " ) ");
        queryPart.append(" OR ( ").append(year).append(" BETWEEN pd." + START_DATE + " AND pd." + EXT_DATE + " )) ");
        return queryPart.toString();
    }

    /**
     * <p>
     * Getter for searchCriteriaType.
     * </p>
     * 
     * @return the searchCriteriaType
     */
    public SearchCriteriaType getSearchCriteriaType()
    {
        return searchCriteriaType;
    }

    /**
     * <p>
     * Setting value for searchCriteriaType.
     * </p>
     * 
     * @param searchCriteriaType
     *            the searchCriteriaType to set
     */
    public void setSearchCriteriaType(SearchCriteriaType searchCriteriaType)
    {
        this.searchCriteriaType = searchCriteriaType;
    }

    public static void main(String[] ar)
    {

        Map<String, String> a = new HashMap<String, String>();
        a.put("CRP1", "2010");
        a.put("CRP2", "2008");
        a.put("CRP3", "2015");
        a.put("CRP4", "");
        a.put("CRP5", "");
        a.put("CRP6", "");

        AdvancedSearchCriteria crt = new AdvancedSearchCriteria();

        List<String> l1 = new ArrayList<String>();
        l1.add("CRP1");
        l1.add("CRP3");
        l1.add("CRP5");
        l1.add("CRP6");
        l1.add("CRP4");

        crt.setPrograms(l1);
        //		crt.setStartDate("2012");

        String query = crt.createProjectSearchCriteria(a);

        System.out.println(":: " + query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("AdvancedSearchCriteria [searchCriteriaType=");
        builder.append(searchCriteriaType);
        builder.append(", startDate=");
        builder.append(startDate);
        builder.append(", programs=");
        builder.append(programs);
        builder.append(", regions=");
        builder.append(regions);
        builder.append(", countries=");
        builder.append(countries);
        builder.append(", donors=");
        builder.append(donors);
        builder.append(", textSearch=");
        builder.append(textSearch);
        builder.append("]");
        return builder.toString();
    }

}
