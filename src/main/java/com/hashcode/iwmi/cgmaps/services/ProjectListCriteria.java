/*
 * FILENAME
 *     ProjectListCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.services;

import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.END_DATE;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.EXT_DATE;
import static com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper.START_DATE;
import static com.hashcode.iwmi.cgmaps.services.ProjectListViewRowMapper.COUNTRY_NAME;
import static com.hashcode.iwmi.cgmaps.services.ProjectListViewRowMapper.PROGRAM;
import static com.hashcode.iwmi.cgmaps.services.ProjectListViewRowMapper.PROJECT_STATE;
import static com.hashcode.iwmi.cgmaps.util.Utilities.isNullOrEmpty;
import static java.lang.Integer.valueOf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Project Lists Criteria.
 * </p>
 * 
 * @author Amila Silva
 * @contact amila@hashcodesys.com
 * @version 1.0
 * 
 */
public class ProjectListCriteria extends SearchCriteria
{

    private static final long serialVersionUID = -3274655425509949501L;
    private static final Logger logger = LoggerFactory.getLogger(ProjectListCriteria.class);

    private String country;
    private String program;
    private String fromYear;
    private String toYear;
    private String projectState;

    /**
     * <p>
     * Getter for country.
     * </p>
     * 
     * @return the country
     */
    public String getCountry()
    {
        return country;
    }

    /**
     * <p>
     * Setting value for country.
     * </p>
     * 
     * @param country
     *            the country to set
     */
    public void setCountry(String country)
    {
        this.country = country;
    }

    /**
     * <p>
     * Getter for program.
     * </p>
     * 
     * @return the program
     */
    public String getProgram()
    {
        return program;
    }

    /**
     * <p>
     * Setting value for program.
     * </p>
     * 
     * @param program
     *            the program to set
     */
    public void setProgram(String program)
    {
        this.program = program;
    }

    /**
     * <p>
     * Getter for fromYear.
     * </p>
     * 
     * @return the fromYear
     */
    public String getFromYear()
    {
        return fromYear;
    }

    /**
     * <p>
     * Setting value for fromYear.
     * </p>
     * 
     * @param fromYear
     *            the fromYear to set
     */
    public void setFromYear(String fromYear)
    {
        this.fromYear = fromYear;
    }

    /**
     * <p>
     * Getter for toYear.
     * </p>
     * 
     * @return the toYear
     */
    public String getToYear()
    {
        return toYear;
    }

    /**
     * <p>
     * Setting value for toYear.
     * </p>
     * 
     * @param toYear
     *            the toYear to set
     */
    public void setToYear(String toYear)
    {
        this.toYear = toYear;
    }

    /**
     * <p>
     * Getter for projectState.
     * </p>
     * 
     * @return the projectState
     */
    public String getProjectState()
    {
        return projectState;
    }

    /**
     * <p>
     * Setting value for projectState.
     * </p>
     * 
     * @param projectState
     *            the projectState to set
     */
    public void setProjectState(String projectState)
    {
        this.projectState = projectState;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hashcode.iwmi.cgmaps.services.SearchCriteria#hasCriteria()
     */
    @Override
    public boolean hasCriteria()
    {
        return !isNullOrEmpty(country) || !isNullOrEmpty(program) || !isNullOrEmpty(fromYear) || !isNullOrEmpty(toYear)
            || !isNullOrEmpty(projectState);
    }

    /**
     * <p>
     * Check for program existence in criteria
     * </p>
     * 
     * @return boolean
     */
    public boolean hasProgram()
    {
        return !isNullOrEmpty(program);
    }

    /**
     * <p>
     * Check for from year existence in criteria
     * </p>
     * 
     * @return boolean
     */
    public boolean hasFromYear()
    {
        return !isNullOrEmpty(fromYear);
    }

    public String getCriteriaQuery(Map<String, String> allProgramsYearMap)
    {
        StringBuilder query = new StringBuilder();
        boolean isAdded = false;

        logger.debug("Before creating the criteria query ");
        if (!isNullOrEmpty(country))
        {
            query.append(" c." + COUNTRY_NAME + " = ").append(" '" + country + "' ");
            isAdded = true;
        }

        isAdded = filterByProgramNYear(allProgramsYearMap, query, isAdded);

        if (!isNullOrEmpty(projectState))
        {
            if (isAdded)
                query.append(" AND ");
            query.append(" pd." + PROJECT_STATE + " = ").append(" '" + projectState + "' ");
            isAdded = true;
        }
        return query.toString();
    }

    private boolean filterByProgramNYear(Map<String, String> allProgramsYearMap, StringBuilder query, boolean isAdded)
    {

        if (hasProgram() && !hasFromYear())
        {
            if (isAdded)
                query.append(" AND ");
            query.append(" pd." + PROGRAM + " = ").append(" '" + program + "' ");

            //			String yrStr = allProgramsYearMap.get(program);
            //
            //			if (!isNullOrEmpty(yrStr)) {
            //				query.append(getQueryPartForAfterFromYear(yrStr));
            //			}

            isAdded = true;
        }
        else if (!hasProgram() && hasFromYear())
        {
            if (isAdded)
                query.append(" AND ");

            query.append(getQueryPartForFromYear(fromYear, false));
            isAdded = true;

        }
        else if (hasProgram() && hasFromYear())
        {
            if (isAdded)
                query.append(" AND ");

            query.append(" pd." + PROGRAM + " = ").append(" '" + program + "' ");

            String yrStr = allProgramsYearMap.get(program);
            int filterYear = (!isNullOrEmpty(yrStr)) ? valueOf(yrStr) : 1900;
            int frmYear = valueOf(fromYear);

            if (frmYear >= filterYear)
            {
                query.append(getQueryPartForFromYear(fromYear, true));
            }
            else
            {
                query.append(getQueryPartForFromYear("4000", true));
            }

            isAdded = true;

        }
        else
        { // add all programs to search with specified year filters

        }
        return isAdded;
    }

    private String getQueryPartForFromYear(String year, boolean needAnd)
    {
        StringBuilder queryPart = new StringBuilder();
        if (needAnd)
            queryPart.append(" AND ");
        queryPart.append(" (( pd." + EXT_DATE + " IS NULL AND ").append(year)
            .append(" BETWEEN  pd." + START_DATE + " AND pd." + END_DATE + " ) ");
        queryPart.append(" OR ( ").append(year).append(" BETWEEN pd." + START_DATE + " AND pd." + EXT_DATE + " )) ");
        return queryPart.toString();
    }

    //	private String getQueryPartForAfterFromYear(String year) {
    //		StringBuilder queryPart = new StringBuilder();
    //		queryPart.append(" AND ");
    //		queryPart.append(" pd." + START_DATE + " >= '").append(year)
    //				.append("' ");
    //		return queryPart.toString();
    //	}

    public static void main(String[] aee)
    {
        ProjectListCriteria crit = new ProjectListCriteria();

        Map<String, String> a = new HashMap<String, String>();
        a.put("CRP1", "2010");
        a.put("CRP2", "2008");
        a.put("CRP3", "2015");
        a.put("CRP4", "");
        a.put("CRP5", "");
        a.put("CRP6", "");

        List<String> l1 = new ArrayList<String>();
        l1.add("CRP1");
        l1.add("CRP3");
        l1.add("CRP5");

        List<String> l2 = new ArrayList<String>();
        l2.add("CRP2");
        l2.add("CRP6");
        l2.add("CRP8");

        List<String> l3 = new ArrayList<String>();
        l3.add("CRP3");
        l3.add("CRP7");
        l3.add("CRP9");

        Map<String, List<String>> b = new HashMap<String, List<String>>();
        b.put("2006", l1);
        b.put("2013", l2);
        b.put("2008", l1);

        // crit.setProgram("CRP2");
        // crit.setFromYear("2010");

        String queryPart = crit.getCriteriaQuery(a);
        System.out.println(":: " + queryPart);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProjectListCriteria [country=");
        builder.append(country);
        builder.append(", program=");
        builder.append(program);
        builder.append(", fromYear=");
        builder.append(fromYear);
        builder.append(", toYear=");
        builder.append(toYear);
        builder.append(", projectState=");
        builder.append(projectState);
        builder.append("]");
        return builder.toString();
    }

}
