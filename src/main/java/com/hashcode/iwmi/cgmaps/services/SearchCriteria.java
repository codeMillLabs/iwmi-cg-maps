/*
 * FILENAME
 *     SearchCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.services;

import java.io.Serializable;

/**
 * <p>
 * Criteria base class for all common functions of search criteria.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 */
public abstract class SearchCriteria implements Serializable {

	private static final long serialVersionUID = 3591553218046728501L;

	private int totalRows = 0;
	private int pageSize = 20;
	private int firstPage = 0;
	private int currentPageNo = 0;
	private int lastPageNo = 0;
	private int totalNoPages = 0;

	/**
	 * 
	 * <p>
	 * method to check for existence of the valid criteria for search.
	 * </p>
	 * 
	 * @return boolean of results of valid criteria.
	 * 
	 */
	public abstract boolean hasCriteria();

//	/**
//	 * <p>
//	 * Create query with the available criteria fields.
//	 * </p>
//	 * 
//	 * 
//	 * @return query part with build criteria.
//	 * 
//	 */
//	public abstract String getCriteriaQuery();

	/**
	 * <p>
	 * Getter for totalRows.
	 * </p>
	 * 
	 * @return the totalRows
	 */
	public int getTotalRows() {
		return totalRows;
	}

	/**
	 * <p>
	 * Setting value for totalRows.
	 * </p>
	 * 
	 * @param totalRows
	 *            the totalRows to set
	 */
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}

	/**
	 * <p>
	 * Getter for pageSize.
	 * </p>
	 * 
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * <p>
	 * Setting value for pageSize.
	 * </p>
	 * 
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * <p>
	 * Getter for firstPage.
	 * </p>
	 * 
	 * @return the firstPage
	 */
	public int getFirstPage() {
		return firstPage;
	}

	/**
	 * <p>
	 * Setting value for firstPage.
	 * </p>
	 * 
	 * @param firstPage
	 *            the firstPage to set
	 */
	public void setFirstPage(int firstPage) {
		this.firstPage = firstPage;
	}

	/**
	 * <p>
	 * Getter for currentPageNo.
	 * </p>
	 * 
	 * @return the currentPageNo
	 */
	public int getCurrentPageNo() {
		return currentPageNo;
	}

	/**
	 * <p>
	 * Setting value for currentPageNo.
	 * </p>
	 * 
	 * @param currentPageNo
	 *            the currentPageNo to set
	 */
	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	/**
	 * <p>
	 * Getter for lastPageNo.
	 * </p>
	 * 
	 * @return the lastPageNo
	 */
	public int getLastPageNo() {
		return lastPageNo;
	}

	/**
	 * <p>
	 * Setting value for lastPageNo.
	 * </p>
	 * 
	 * @param lastPageNo
	 *            the lastPageNo to set
	 */
	public void setLastPageNo(int lastPageNo) {
		this.lastPageNo = lastPageNo;
	}

	public void setInitalPagination(int totalDataSize) {
		this.totalRows = totalDataSize;
		this.totalNoPages = ((totalRows % pageSize) == 0) ? totalRows
				/ pageSize : (totalRows / pageSize) + 1;
	}

}
