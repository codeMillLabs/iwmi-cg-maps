/*
 * FILENAME
 *     ProjectDetailQueryService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.services;

import java.util.List;
import java.util.Map;

import com.hashcode.iwmi.cgmaps.model.ProjectDetailView;
import com.hashcode.iwmi.cgmaps.model.ProjectDocsView;
import com.hashcode.iwmi.cgmaps.model.ProjectListView;

/**
 * 
 * <p>
 * Interface service for the Project details query handling from the back end.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 */
public interface ProjectDetailQueryService
{

    /**
     * <p>
     * Fetch the Project List view details according to the given criteria.
     * </p>
     * 
     * @param criteria
     *            search criteria
     * @return list of project list view
     */
    List<ProjectListView> fetchProjectListView(SearchCriteria criteria);

    /**
     * <p>
     * Fetch the project related documents according to the given criteria.
     * </p>
     * 
     * @param criteria
     *            search criteria
     * @return list of project list view
     */
    ProjectDocsView fetchProjectDocuments(SearchCriteria criteria);

    /**
     * <p>
     * Get all project distribution on all available countries.
     * </p>
     * 
     * 
     * @param criteria
     *            search criteria
     * @return Map of details related to project distribution.
     * 
     */
    Map<String, Double> getProjectDistributionOnCountries(SearchCriteria criteria);

    /**
     * 
     * <p>
     * Find Project details by Id.
     * </p>
     * 
     * @param projectId
     *            project id
     * @return {@link ProjectDetailView}
     * 
     */
    ProjectDetailView findProjectDetailsById(String projectId);

}
