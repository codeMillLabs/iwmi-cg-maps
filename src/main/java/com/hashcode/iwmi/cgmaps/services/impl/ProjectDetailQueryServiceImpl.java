/*
 * FILENAME
 *     ProjectDetailQueryServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.services.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao;
import com.hashcode.iwmi.cgmaps.dao.ProjectOutcomeQueryDao;
import com.hashcode.iwmi.cgmaps.model.ProjectDetailView;
import com.hashcode.iwmi.cgmaps.model.ProjectDocsView;
import com.hashcode.iwmi.cgmaps.model.ProjectInfoView;
import com.hashcode.iwmi.cgmaps.model.ProjectListView;
import com.hashcode.iwmi.cgmaps.model.ProjectOutcomeView;
import com.hashcode.iwmi.cgmaps.services.ProjectDetailQueryService;
import com.hashcode.iwmi.cgmaps.services.ProjectListCriteria;
import com.hashcode.iwmi.cgmaps.services.SearchCriteria;

/**
 * <p>
 * Implementation of the Project Detail query Service.
 * </P>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 */
@Service("ProjectDetailQueryService")
public class ProjectDetailQueryServiceImpl implements ProjectDetailQueryService {

	private static final Logger logger = LoggerFactory
			.getLogger(ProjectDetailQueryServiceImpl.class);

	@Autowired
	private ProjectInfoQueryDao projectInfoQueryDao;
	
	@Autowired
	private ProjectOutcomeQueryDao projectOutcomeQueryDao;

	public List<ProjectListView> fetchProjectListView(SearchCriteria criteria) {
		logger.info("Project List view data search according to the query");
		return projectInfoQueryDao.fetchProjectListView(criteria);
	}

	public ProjectDocsView fetchProjectDocuments(SearchCriteria criteria) {
		logger.info("fetchProjectDocuments");
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.hashcode.iwmi.cgmaps.services.ProjectDetailQueryService#getProjectDistributionOnCountries(com.hashcode.iwmi.cgmaps.services.SearchCriteria)
	 */
	public Map<String, Double> getProjectDistributionOnCountries(
			SearchCriteria criteria) {
		String countryName = null;
		ProjectListCriteria projCriteria = null;
		// get total project for country
		if (criteria != null && criteria.hasCriteria()) {
			projCriteria = (ProjectListCriteria) criteria;
			countryName = projCriteria.getCountry();
		}

		logger.info("Criteria for the retrive the project distributions : {}",
				projCriteria);

		double totalProjects = projectInfoQueryDao
				.getProjectCountByCountry(countryName);
		Map<String, Integer> countryToProjCountMap = projectInfoQueryDao
				.getProjectsForCountries(projCriteria);

		logger.debug("Total no of Projects : {}, Project Distrib count :{}",
				totalProjects, countryToProjCountMap.size());

		Map<String, Double> projectDistributionMap = new HashMap<String, Double>();

		for (Map.Entry<String, Integer> entry : countryToProjCountMap
				.entrySet()) {
			String country = entry.getKey();
			double count = entry.getValue();

			double presentageValue = 0;

			if (totalProjects > 0) {
				presentageValue = BigDecimal
						.valueOf((count / totalProjects) * 100)
						.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			}
			logger.debug(
					"Project to country distribution. [Country : {}, \t No of Projects : {}, \t Distribution : {} ]",
					new Object[] { country, count, presentageValue });

			projectDistributionMap.put(country, presentageValue);
		}

		logger.info("Project Distribution loaded : {}",
				projectDistributionMap.size());

		return projectDistributionMap;
	}
	
	

	/**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.cgmaps.services.ProjectDetailQueryService#findProjectDetailsById(java.lang.String)
     */
    public ProjectDetailView findProjectDetailsById(String projectId)
    {
        ProjectDetailView detailView = new ProjectDetailView();
        ProjectInfoView infoView = projectInfoQueryDao.fingProjectById(projectId);
        List<ProjectOutcomeView> outcomeView = projectOutcomeQueryDao.findProjectOutcomeView(projectId);
        
        detailView.setProjectInfo(infoView);
        detailView.setProjectOutcome(outcomeView);
        return detailView;
    }

    /**
     * <p>
     * Getter for projectInfoQueryDao.
     * </p>
     * 
     * @return the projectInfoQueryDao
     */
    public ProjectInfoQueryDao getProjectInfoQueryDao()
    {
        return projectInfoQueryDao;
    }

    /**
     * <p>
     * Setting value for projectInfoQueryDao.
     * </p>
     * 
     * @param projectInfoQueryDao the projectInfoQueryDao to set
     */
    public void setProjectInfoQueryDao(ProjectInfoQueryDao projectInfoQueryDao)
    {
        this.projectInfoQueryDao = projectInfoQueryDao;
    }

    /**
     * <p>
     * Getter for projectOutcomeQueryDao.
     * </p>
     * 
     * @return the projectOutcomeQueryDao
     */
    public ProjectOutcomeQueryDao getProjectOutcomeQueryDao()
    {
        return projectOutcomeQueryDao;
    }

    /**
     * <p>
     * Setting value for projectOutcomeQueryDao.
     * </p>
     * 
     * @param projectOutcomeQueryDao the projectOutcomeQueryDao to set
     */
    public void setProjectOutcomeQueryDao(ProjectOutcomeQueryDao projectOutcomeQueryDao)
    {
        this.projectOutcomeQueryDao = projectOutcomeQueryDao;
    }
}
