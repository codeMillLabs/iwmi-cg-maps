/*
 * FILENAME
 *     AdvanceSearchServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao;
import com.hashcode.iwmi.cgmaps.model.ProjectInfoView;
import com.hashcode.iwmi.cgmaps.services.AdvanceSearchService;
import com.hashcode.iwmi.cgmaps.services.AdvancedSearchCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Advance search service implementation.
 * </p>
 *
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
@Service("AdvanceSearchService")
@Transactional
public class AdvanceSearchServiceImpl implements AdvanceSearchService
{
    @Autowired
    private ProjectInfoQueryDao projectInfoQueryDao;
    
    public List<ProjectInfoView> searchProjects(AdvancedSearchCriteria criteria)
    {
        return projectInfoQueryDao.fetchProjectInfo(criteria);
    }

    /**
     * <p>
     * Setting value for projectInfoQueryDao.
     * </p>
     * 
     * @param projectInfoQueryDao the projectInfoQueryDao to set
     */
    public void setProjectInfoQueryDao(ProjectInfoQueryDao projectInfoQueryDao)
    {
        this.projectInfoQueryDao = projectInfoQueryDao;
    }
    
}
