/*
 * FILENAME
 *     ProjectOutcomeCriteria.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.services;

/**
 * <p>
 * Class for Project Outcome criteria.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 */
public class ProjectOutcomeCriteria extends SearchCriteria {

	private static final long serialVersionUID = -1125355663855816319L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hashcode.iwmi.cgmaps.services.SearchCriteria#hasCriteria()
	 */
	@Override
	public boolean hasCriteria() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hashcode.iwmi.cgmaps.services.SearchCriteria#getCriteriaQuery()
	 */
	public String getCriteriaQuery() {
		// TODO Auto-generated method stub
		return null;
	}

}
