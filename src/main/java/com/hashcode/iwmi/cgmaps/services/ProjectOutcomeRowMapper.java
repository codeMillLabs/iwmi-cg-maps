/*
 * FILENAME
 *     ProjectOutcomeRowMapper.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.services;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hashcode.iwmi.cgmaps.model.ProjectOutcomeView;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Project outcome implementation for Row Mapping.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
public class ProjectOutcomeRowMapper implements RowMapper<ProjectOutcomeView> {

	public static final String ITEM_NO = "ItemNo";
	public static final String OUTPUT_ID = "OutputId";
//	public static final String MAIN_PROJ_ID = "MainProjectID";
	public static final String SUB_PROJ_ID = "SubProjectID";
	public static final String SUB_PROJ_NAME = "SubProjectName";
	public static final String YEAR = "Year";
	public static final String NAME = "Name";
	public static final String DUE_DATE = "DueDate";
	public static final String ORG_DUE_DATE = "OrgDueDate";
	public static final String PO_COMMENTS = "POComments";
	public static final String RESPONSIBLE = "Responsible";
	public static final String MTP_REF = "MTPRef";
	public static final String STATUS = "Status";
	
	public static final String PD_SHORT_NAME = "pdShrtName";
	public static final String PD_DESC = "pdDesc";
	public static final String PD_DONORS = "pdDonors";
	public static final String PD_LOC = "pdLoc";
	public static final String PD_PROG = "pdProg";
	public static final String PD_WEB_TITLE = "pdWebTitle";
	public static final String PD_START_DATE = "pdStartDate";
	public static final String PD_END_DATE = "pdEndDate";

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	public ProjectOutcomeView mapRow(ResultSet rs, int rowNum)
			throws SQLException {

		ProjectOutcomeView view = new ProjectOutcomeView();
		view.setItemNo(rs.getString(ITEM_NO));
		view.setOutputId(rs.getString(OUTPUT_ID));
//		view.setMainProjId(rs.getString(MAIN_PROJ_ID));
		view.setSubProjectId(rs.getString(SUB_PROJ_ID));
		view.setProjectName(rs.getString(SUB_PROJ_NAME));
		view.setName(rs.getString(NAME));
		view.setYear(rs.getString(YEAR));
		view.setDueDate(rs.getString(DUE_DATE));
		view.setOrgDueDate(rs.getString(ORG_DUE_DATE));
		view.setPoComments(rs.getString(PO_COMMENTS));
		view.setResponsible(rs.getString(RESPONSIBLE));
		view.setMtpRef(rs.getString(MTP_REF));
		view.setStatus(rs.getString(STATUS));
		view.setProgram(rs.getString(PD_PROG));
		view.setWebTitle(rs.getString(PD_WEB_TITLE));

		return view;
	}

}
