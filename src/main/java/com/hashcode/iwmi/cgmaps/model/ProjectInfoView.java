/**
 * 
 */
package com.hashcode.iwmi.cgmaps.model;

import static com.hashcode.iwmi.cgmaps.util.Utilities.isNullOrEmpty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * Project detail object.
 * </p>
 * 
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 */
public class ProjectInfoView implements Serializable
{
    private static final long serialVersionUID = 8266729274669317961L;
    private Long projectId;
    private String shortName;
    private String longName;
    private String code;
    private String description;
    private Date startDate;
    private Date endDate;
    private Date extendedDate;
    private List<String> donors;
    private String donorsStr;
    private List<String> locations;
    private String locationStr;
    private String theme;
    private String program;
    private String region;
    private String projectLeader;
    private String plEmail;
    private String webTitle;
    private String webAddress;
    private String projectImage;
    private String contactName;
    private String contactEmail;
    private String projectStatus;

    /**
     * <p>
     * Getter for projectId.
     * </p>
     * 
     * @return the projectId
     */
    public Long getProjectId()
    {
        return projectId;
    }

    /**
     * <p>
     * Setting value for projectId.
     * </p>
     * 
     * @param projectId
     *            the projectId to set
     */
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    /**
     * <p>
     * Getter for shortName.
     * </p>
     * 
     * @return the shortName
     */
    public String getShortName()
    {
        return shortName;
    }

    /**
     * <p>
     * Setting value for shortName.
     * </p>
     * 
     * @param shortName
     *            the shortName to set
     */
    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    /**
     * <p>
     * Getter for longName.
     * </p>
     * 
     * @return the longName
     */
    public String getLongName()
    {
        return longName;
    }

    /**
     * <p>
     * Setting value for longName.
     * </p>
     * 
     * @param longName
     *            the longName to set
     */
    public void setLongName(String longName)
    {
        this.longName = longName;
    }

    /**
     * <p>
     * Getter for code.
     * </p>
     * 
     * @return the code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * <p>
     * Setting value for code.
     * </p>
     * 
     * @param code
     *            the code to set
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * <p>
     * Getter for description.
     * </p>
     * 
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * <p>
     * Setting value for description.
     * </p>
     * 
     * @param description
     *            the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * <p>
     * Getter for startDate.
     * </p>
     * 
     * @return the startDate
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * <p>
     * Setting value for startDate.
     * </p>
     * 
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * <p>
     * Getter for endDate.
     * </p>
     * 
     * @return the endDate
     */
    public Date getEndDate()
    {
        return endDate;
    }

    /**
     * <p>
     * Setting value for endDate.
     * </p>
     * 
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    /**
     * <p>
     * Getter for extendedDate.
     * </p>
     * 
     * @return the extendedDate
     */
    public Date getExtendedDate()
    {
        return extendedDate;
    }

    /**
     * <p>
     * Setting value for extendedDate.
     * </p>
     * 
     * @param extendedDate
     *            the extendedDate to set
     */
    public void setExtendedDate(Date extendedDate)
    {
        this.extendedDate = extendedDate;
    }

    /**
     * <p>
     * Getter for donors.
     * </p>
     * 
     * @return the donors
     */
    public List<String> getDonors()
    {
        return donors;
    }

    /**
     * <p>
     * Setting value for donors.
     * </p>
     * 
     * @param donors
     *            the donors to set
     */
    public void setDonors(List<String> donors)
    {
        this.donors = donors;
    }

    /**
     * <p>
     * Getter for locations.
     * </p>
     * 
     * @return the locations
     */
    public List<String> getLocations()
    {
        return locations;
    }

    /**
     * <p>
     * Setting value for locations.
     * </p>
     * 
     * @param locations
     *            the locations to set
     */
    public void setLocations(List<String> locations)
    {
        this.locations = locations;
    }

    /**
     * <p>
     * Getter for theme.
     * </p>
     * 
     * @return the theme
     */
    public String getTheme()
    {
        return theme;
    }

    /**
     * <p>
     * Setting value for theme.
     * </p>
     * 
     * @param theme
     *            the theme to set
     */
    public void setTheme(String theme)
    {
        this.theme = theme;
    }

    /**
     * <p>
     * Getter for program.
     * </p>
     * 
     * @return the program
     */
    public String getProgram()
    {
        return program;
    }

    /**
     * <p>
     * Setting value for program.
     * </p>
     * 
     * @param program
     *            the program to set
     */
    public void setProgram(String program)
    {
        this.program = program;
    }

    /**
     * <p>
     * Getter for region.
     * </p>
     * 
     * @return the region
     */
    public String getRegion()
    {
        return region;
    }

    /**
     * <p>
     * Setting value for region.
     * </p>
     * 
     * @param region
     *            the region to set
     */
    public void setRegion(String region)
    {
        this.region = region;
    }

    /**
     * <p>
     * Getter for projectLeader.
     * </p>
     * 
     * @return the projectLeader
     */
    public String getProjectLeader()
    {
        return projectLeader;
    }

    /**
     * <p>
     * Setting value for projectLeader.
     * </p>
     * 
     * @param projectLeader
     *            the projectLeader to set
     */
    public void setProjectLeader(String projectLeader)
    {
        this.projectLeader = projectLeader;
    }

    /**
     * <p>
     * Getter for plEmail.
     * </p>
     * 
     * @return the plEmail
     */
    public String getPlEmail()
    {
        return plEmail;
    }

    /**
     * <p>
     * Setting value for plEmail.
     * </p>
     * 
     * @param plEmail
     *            the plEmail to set
     */
    public void setPlEmail(String plEmail)
    {
        this.plEmail = plEmail;
    }

    /**
     * <p>
     * Getter for webTitle.
     * </p>
     * 
     * @return the webTitle
     */
    public String getWebTitle()
    {
        return webTitle;
    }

    /**
     * <p>
     * Setting value for webTitle.
     * </p>
     * 
     * @param webTitle
     *            the webTitle to set
     */
    public void setWebTitle(String webTitle)
    {
        this.webTitle = webTitle;
    }

    /**
     * <p>
     * Getter for webAddress.
     * </p>
     * 
     * @return the webAddress
     */
    public String getWebAddress()
    {
        return webAddress;
    }

    /**
     * <p>
     * Setting value for webAddress.
     * </p>
     * 
     * @param webAddress
     *            the webAddress to set
     */
    public void setWebAddress(String webAddress)
    {
        this.webAddress = webAddress;
    }

    /**
     * <p>
     * Getter for projectImage.
     * </p>
     * 
     * @return the projectImage
     */
    public String getProjectImage()
    {
        return projectImage;
    }

    /**
     * <p>
     * Setting value for projectImage.
     * </p>
     * 
     * @param projectImage
     *            the projectImage to set
     */
    public void setProjectImage(String projectImage)
    {
        this.projectImage = projectImage;
    }

    /**
     * <p>
     * Getter for contactName.
     * </p>
     * 
     * @return the contactName
     */
    public String getContactName()
    {
        return contactName;
    }

    /**
     * <p>
     * Setting value for contactName.
     * </p>
     * 
     * @param contactName
     *            the contactName to set
     */
    public void setContactName(String contactName)
    {
        this.contactName = contactName;
    }

    /**
     * <p>
     * Getter for contactEmail.
     * </p>
     * 
     * @return the contactEmail
     */
    public String getContactEmail()
    {
        return contactEmail;
    }

    /**
     * <p>
     * Setting value for contactEmail.
     * </p>
     * 
     * @param contactEmail
     *            the contactEmail to set
     */
    public void setContactEmail(String contactEmail)
    {
        this.contactEmail = contactEmail;
    }

    /**
     * <p>
     * Getter for projectStatus.
     * </p>
     * 
     * @return the projectStatus
     */
    public String getProjectStatus()
    {
        return projectStatus;
    }

    /**
     * <p>
     * Setting value for projectStatus.
     * </p>
     * 
     * @param projectStatus
     *            the projectStatus to set
     */
    public void setProjectStatus(String projectStatus)
    {
        this.projectStatus = projectStatus;
    }

    /**
     * <p>
     * Getter for donorsStr.
     * </p>
     * 
     * @return the donorsStr
     */
    public String getDonorsStr()
    {
        return donorsStr;
    }

    /**
     * <p>
     * Setting value for donorsStr.
     * </p>
     * 
     * @param donorsStr
     *            the donorsStr to set
     */
    public void setDonorsStr(String donorsStr)
    {
        this.donorsStr = donorsStr;

        if (isNullOrEmpty(donorsStr))
        {
            this.donorsStr = "";
            return;
        }

        int lenght = donorsStr.trim().length();
        String lastChar = (isNullOrEmpty(donorsStr)) ? "" : donorsStr.trim().substring(lenght - 1);

        if (lastChar.equals(","))
        {
            this.donorsStr = donorsStr.trim().substring(0, lenght - 1);
        }
        else
        {
            this.donorsStr = donorsStr;
        }
    }

    /**
     * <p>
     * Getter for locationStr.
     * </p>
     * 
     * @return the locationStr
     */
    public String getLocationStr()
    {
        return locationStr;
    }

    /**
     * <p>
     * Setting value for locationStr.
     * </p>
     * 
     * @param locationStr
     *            the locationStr to set
     */
    public void setLocationStr(String locationStr)
    {
        if (isNullOrEmpty(locationStr))
        {
            this.locationStr = "";
            return;
        }

        int lenght = locationStr.trim().length();
        String lastChar = (isNullOrEmpty(locationStr)) ? "" : locationStr.trim().substring(lenght - 1);

        if (lastChar.equals(","))
        {
            this.locationStr = locationStr.trim().substring(0, lenght - 1);
        }
        else
        {
            this.locationStr = locationStr;
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProjectListView [projectId=");
        builder.append(projectId);
        builder.append(", shortName=");
        builder.append(shortName);
        builder.append(", longName=");
        builder.append(longName);
        builder.append(", code=");
        builder.append(code);
        builder.append(", description=");
        builder.append(description);
        builder.append(", startDate=");
        builder.append(startDate);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append(", extendedDate=");
        builder.append(extendedDate);
        builder.append(", donors=");
        builder.append(donors);
        builder.append(", locations=");
        builder.append(locations);
        builder.append(", theme=");
        builder.append(theme);
        builder.append(", program=");
        builder.append(program);
        builder.append(", region=");
        builder.append(region);
        builder.append(", projectLeader=");
        builder.append(projectLeader);
        builder.append(", plEmail=");
        builder.append(plEmail);
        builder.append(", webTitle=");
        builder.append(webTitle);
        builder.append(", webAddress=");
        builder.append(webAddress);
        builder.append(", projectImage=");
        builder.append(projectImage);
        builder.append(", contactName=");
        builder.append(contactName);
        builder.append(", contactEmail=");
        builder.append(contactEmail);
        builder.append(", projectStatus=");
        builder.append(projectStatus);
        builder.append("]");
        return builder.toString();
    }

}
