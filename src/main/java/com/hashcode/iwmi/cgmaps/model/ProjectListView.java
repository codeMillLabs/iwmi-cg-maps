/**
 * 
 */
package com.hashcode.iwmi.cgmaps.model;

import java.io.Serializable;

/**
 * 
 * <p>
 * Project Detail list view data holder
 * <p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 * 
 */
public class ProjectListView implements Serializable
{
    private static final long serialVersionUID = 7610718044731983640L;
    private String projectId;
    private String program;
    private String theme;
    private String webTitle;
    private String projectImage;
    private String projectStatus;

    /**
     * <p>
     * Getter for projectId.
     * </p>
     * 
     * @return the projectId
     */
    public String getProjectId()
    {
        return projectId;
    }

    /**
     * <p>
     * Setting value for projectId.
     * </p>
     * 
     * @param projectId
     *            the projectId to set
     */
    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    /**
     * <p>
     * Getter for program.
     * </p>
     * 
     * @return the program
     */
    public String getProgram()
    {
        return program;
    }

    /**
     * <p>
     * Setting value for program.
     * </p>
     * 
     * @param program
     *            the program to set
     */
    public void setProgram(String program)
    {
        this.program = program;
    }

    /**
     * <p>
     * Getter for theme.
     * </p>
     * 
     * @return the theme
     */
    public String getTheme()
    {
        return theme;
    }

    /**
     * <p>
     * Setting value for theme.
     * </p>
     * 
     * @param theme
     *            the theme to set
     */
    public void setTheme(String theme)
    {
        this.theme = theme;
    }

    /**
     * <p>
     * Getter for webTitle.
     * </p>
     * 
     * @return the webTitle
     */
    public String getWebTitle()
    {
        return webTitle;
    }

    /**
     * <p>
     * Setting value for webTitle.
     * </p>
     * 
     * @param webTitle
     *            the webTitle to set
     */
    public void setWebTitle(String webTitle)
    {
        this.webTitle = webTitle;
    }

    /**
     * <p>
     * Getter for projectImage.
     * </p>
     * 
     * @return the projectImage
     */
    public String getProjectImage()
    {
        return projectImage;
    }

    /**
     * <p>
     * Setting value for projectImage.
     * </p>
     * 
     * @param projectImage
     *            the projectImage to set
     */
    public void setProjectImage(String projectImage)
    {
        this.projectImage = projectImage;
    }

    /**
     * <p>
     * Getter for projectStatus.
     * </p>
     * 
     * @return the projectStatus
     */
    public String getProjectStatus()
    {
        return projectStatus;
    }

    /**
     * <p>
     * Setting value for projectStatus.
     * </p>
     * 
     * @param projectStatus
     *            the projectStatus to set
     */
    public void setProjectStatus(String projectStatus)
    {
        this.projectStatus = projectStatus;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProjectListView [projectId=");
        builder.append(projectId);
        builder.append(", program=");
        builder.append(program);
        builder.append(", theme=");
        builder.append(theme);
        builder.append(", webTitle=");
        builder.append(webTitle);
        builder.append("]");
        return builder.toString();
    }

}
