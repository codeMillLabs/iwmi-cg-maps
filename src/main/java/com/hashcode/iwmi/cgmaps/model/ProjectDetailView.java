/*
 * FILENAME
 *     ProjectDetailView.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * This class will holds the project details and other all related informations.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
public class ProjectDetailView implements Serializable
{
    private static final long serialVersionUID = -4058548940197519585L;

    private ProjectInfoView projectInfo;
    private List<ProjectOutcomeView> projectOutcome = new ArrayList<ProjectOutcomeView>();

    /**
     * <p>
     * Getter for projectInfo.
     * </p>
     * 
     * @return the projectInfo
     */
    public ProjectInfoView getProjectInfo()
    {
        return projectInfo;
    }

    /**
     * <p>
     * Setting value for projectInfo.
     * </p>
     * 
     * @param projectInfo
     *            the projectInfo to set
     */
    public void setProjectInfo(ProjectInfoView projectInfo)
    {
        this.projectInfo = projectInfo;
    }

    /**
     * <p>
     * Getter for projectOutcome.
     * </p>
     * 
     * @return the projectOutcome
     */
    public List<ProjectOutcomeView> getProjectOutcome()
    {
        return projectOutcome;
    }

    /**
     * <p>
     * Setting value for projectOutcome.
     * </p>
     * 
     * @param projectOutcome
     *            the projectOutcome to set
     */
    public void setProjectOutcome(List<ProjectOutcomeView> projectOutcome)
    {
        this.projectOutcome = projectOutcome;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProjectDetailView [projectInfo=");
        builder.append(projectInfo);
        builder.append(", projectOutcome=");
        builder.append(projectOutcome);
        builder.append("]");
        return builder.toString();
    }
}
