/**
 * 
 */
package com.hashcode.iwmi.cgmaps.model;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Project Docs view object
 * </p>
 *
 *
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 *
 */
public class ProjectDocsView {
	
	Map<String, List<ProjectInfoView>> projectDocs;

	/**
	 * @return the projectDocs
	 */
	public Map<String, List<ProjectInfoView>> getProjectDocs() {
		return projectDocs;
	}

	/**
	 * @param projectDocs the projectDocs to set
	 */
	public void setProjectDocs(Map<String, List<ProjectInfoView>> projectDocs) {
		this.projectDocs = projectDocs;
	}
	
	

}
