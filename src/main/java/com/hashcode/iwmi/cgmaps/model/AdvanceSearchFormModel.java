/*
 * FILENAME
 *     AdvanceSearchFormModel.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.model;


//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Common form model for Advance search criteria form.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
public class AdvanceSearchFormModel
{

    private String startDate;
    private String[] programs;
    private String[] regions;
    private String[] countries;
    private String[] donors;
    private String[] outputTargetYears;
    private String[] outputTargetTypes;
    private String textSearch;
    private String txtInTargetDesc;
    private String txtInOutputDesc;
    private String programFilter;

    /**
     * <p>
     * Getter for startDate.
     * </p>
     * 
     * @return the startDate
     */
    public String getStartDate()
    {
        return startDate;
    }

    /**
     * <p>
     * Setting value for startDate.
     * </p>
     * 
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    /**
     * <p>
     * Getter for programs.
     * </p>
     * 
     * @return the programs
     */
    public String[] getPrograms()
    {
        return programs;
    }

    /**
     * <p>
     * Setting value for programs.
     * </p>
     * 
     * @param programs
     *            the programs to set
     */
    public void setPrograms(String[] programs)
    {
        this.programs = programs;
    }

    /**
     * <p>
     * Getter for regions.
     * </p>
     * 
     * @return the regions
     */
    public String[] getRegions()
    {
        return regions;
    }

    /**
     * <p>
     * Setting value for regions.
     * </p>
     * 
     * @param regions
     *            the regions to set
     */
    public void setRegions(String[] regions)
    {
        this.regions = regions;
    }

    /**
     * <p>
     * Getter for countries.
     * </p>
     * 
     * @return the countries
     */
    public String[] getCountries()
    {
        return countries;
    }

    /**
     * <p>
     * Setting value for countries.
     * </p>
     * 
     * @param countries
     *            the countries to set
     */
    public void setCountries(String[] countries)
    {
        this.countries = countries;
    }

    /**
     * <p>
     * Getter for donors.
     * </p>
     * 
     * @return the donors
     */
    public String[] getDonors()
    {
        return donors;
    }

    /**
     * <p>
     * Setting value for donors.
     * </p>
     * 
     * @param donors
     *            the donors to set
     */
    public void setDonors(String[] donors)
    {
        this.donors = donors;
    }

    /**
     * <p>
     * Getter for outputTargetYears.
     * </p>
     * 
     * @return the outputTargetYears
     */
    public String[] getOutputTargetYears()
    {
        return outputTargetYears;
    }

    /**
     * <p>
     * Setting value for outputTargetYears.
     * </p>
     * 
     * @param outputTargetYears
     *            the outputTargetYears to set
     */
    public void setOutputTargetYears(String[] outputTargetYears)
    {
        this.outputTargetYears = outputTargetYears;
    }

    /**
     * <p>
     * Getter for outputTargetTypes.
     * </p>
     * 
     * @return the outputTargetTypes
     */
    public String[] getOutputTargetTypes()
    {
        return outputTargetTypes;
    }

    /**
     * <p>
     * Setting value for outputTargetTypes.
     * </p>
     * 
     * @param outputTargetTypes
     *            the outputTargetTypes to set
     */
    public void setOutputTargetTypes(String[] outputTargetTypes)
    {
        this.outputTargetTypes = outputTargetTypes;
    }

    /**
     * <p>
     * Getter for textSearch.
     * </p>
     * 
     * @return the textSearch
     */
    public String getTextSearch()
    {
        return textSearch;
    }

    /**
     * <p>
     * Setting value for textSearch.
     * </p>
     * 
     * @param textSearch
     *            the textSearch to set
     */
    public void setTextSearch(String textSearch)
    {
        this.textSearch = textSearch;
    }

    /**
     * <p>
     * Getter for txtInTargetDesc.
     * </p>
     * 
     * @return the txtInTargetDesc
     */
    public String getTxtInTargetDesc()
    {
        return txtInTargetDesc;
    }

    /**
     * <p>
     * Setting value for txtInTargetDesc.
     * </p>
     * 
     * @param txtInTargetDesc
     *            the txtInTargetDesc to set
     */
    public void setTxtInTargetDesc(String txtInTargetDesc)
    {
        this.txtInTargetDesc = txtInTargetDesc;
    }

    /**
     * <p>
     * Getter for txtInOutputDesc.
     * </p>
     * 
     * @return the txtInOutputDesc
     */
    public String getTxtInOutputDesc()
    {
        return txtInOutputDesc;
    }

    /**
     * <p>
     * Setting value for txtInOutputDesc.
     * </p>
     * 
     * @param txtInOutputDesc
     *            the txtInOutputDesc to set
     */
    public void setTxtInOutputDesc(String txtInOutputDesc)
    {
        this.txtInOutputDesc = txtInOutputDesc;
    }

    /**
     * <p>
     * Getter for programFilter.
     * </p>
     * 
     * @return the programFilter
     */
    public String getProgramFilter()
    {
        return programFilter;
    }

    /**
     * <p>
     * Setting value for programFilter.
     * </p>
     * 
     * @param programFilter the programFilter to set
     */
    public void setProgramFilter(String programFilter)
    {
        this.programFilter = programFilter;
    }
    
}
