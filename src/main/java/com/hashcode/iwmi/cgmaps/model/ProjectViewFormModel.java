/*
 * FILENAME
 *     ProjectListViewFormModel.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.model;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Form backing class for the project list view search form.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
public class ProjectViewFormModel
{
    private String program;
    private String country;
    private String year;

    /**
     * <p>
     * Getter for program.
     * </p>
     * 
     * @return the program
     */
    public String getProgram()
    {
        return program;
    }

    /**
     * <p>
     * Setting value for program.
     * </p>
     * 
     * @param program
     *            the program to set
     */
    public void setProgram(String program)
    {
        this.program = program;
    }

    /**
     * <p>
     * Getter for country.
     * </p>
     * 
     * @return the country
     */
    public String getCountry()
    {
        return country;
    }

    /**
     * <p>
     * Setting value for country.
     * </p>
     * 
     * @param country
     *            the country to set
     */
    public void setCountry(String country)
    {
        this.country = country;
    }

    /**
     * <p>
     * Getter for year.
     * </p>
     * 
     * @return the year
     */
    public String getYear()
    {
        return year;
    }

    /**
     * <p>
     * Setting value for year.
     * </p>
     * 
     * @param year
     *            the year to set
     */
    public void setYear(String year)
    {
        this.year = year;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProjectListViewFormModel [program=");
        builder.append(program);
        builder.append(", country=");
        builder.append(country);
        builder.append(", year=");
        builder.append(year);
        builder.append("]");
        return builder.toString();
    }

}
