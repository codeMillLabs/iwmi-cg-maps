/*
 * FILENAME
 *     ProjectOutcomeView.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.model;

import java.io.Serializable;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Project outcome view.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
public class ProjectOutcomeView implements Serializable
{
    private static final long serialVersionUID = -6778333144948999958L;

    private String itemNo;
    private String outputId;
    private String mainProjId;
    private String subProjectId;
    private String projectName;
    private String name;
    private String year;
    private String dueDate;
    private String orgDueDate;
    private String poComments;
    private String responsible;
    private String mtpRef;
    private String status;
    private String shortName;
    private String program;
    private String description;
    private String locations;
    private String webTitle;
    private String donors;
    private String outputName;

    /**
     * <p>
     * Getter for itemNo.
     * </p>
     * 
     * @return the itemNo
     */
    public String getItemNo()
    {
        return itemNo;
    }

    /**
     * <p>
     * Setting value for itemNo.
     * </p>
     * 
     * @param itemNo
     *            the itemNo to set
     */
    public void setItemNo(String itemNo)
    {
        this.itemNo = itemNo;
    }

    /**
     * <p>
     * Getter for outputId.
     * </p>
     * 
     * @return the outputId
     */
    public String getOutputId()
    {
        return outputId;
    }

    /**
     * <p>
     * Setting value for outputId.
     * </p>
     * 
     * @param outputId
     *            the outputId to set
     */
    public void setOutputId(String outputId)
    {
        this.outputId = outputId;
    }

    /**
     * <p>
     * Getter for mainProjId.
     * </p>
     * 
     * @return the mainProjId
     */
    public String getMainProjId()
    {
        return mainProjId;
    }

    /**
     * <p>
     * Setting value for mainProjId.
     * </p>
     * 
     * @param mainProjId
     *            the mainProjId to set
     */
    public void setMainProjId(String mainProjId)
    {
        this.mainProjId = mainProjId;
    }

    /**
     * <p>
     * Getter for subProjectId.
     * </p>
     * 
     * @return the subProjectId
     */
    public String getSubProjectId()
    {
        return subProjectId;
    }

    /**
     * <p>
     * Setting value for subProjectId.
     * </p>
     * 
     * @param subProjectId
     *            the subProjectId to set
     */
    public void setSubProjectId(String subProjectId)
    {
        this.subProjectId = subProjectId;
    }

    /**
     * <p>
     * Getter for projectName.
     * </p>
     * 
     * @return the projectName
     */
    public String getProjectName()
    {
        return projectName;
    }

    /**
     * <p>
     * Setting value for projectName.
     * </p>
     * 
     * @param projectName
     *            the projectName to set
     */
    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    /**
     * <p>
     * Getter for name.
     * </p>
     * 
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * <p>
     * Setting value for name.
     * </p>
     * 
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * <p>
     * Getter for year.
     * </p>
     * 
     * @return the year
     */
    public String getYear()
    {
        return year;
    }

    /**
     * <p>
     * Setting value for year.
     * </p>
     * 
     * @param year
     *            the year to set
     */
    public void setYear(String year)
    {
        this.year = year;
    }

    /**
     * <p>
     * Getter for dueDate.
     * </p>
     * 
     * @return the dueDate
     */
    public String getDueDate()
    {
        return dueDate;
    }

    /**
     * <p>
     * Setting value for dueDate.
     * </p>
     * 
     * @param dueDate
     *            the dueDate to set
     */
    public void setDueDate(String dueDate)
    {
        this.dueDate = dueDate;
    }

    /**
     * <p>
     * Getter for orgDueDate.
     * </p>
     * 
     * @return the orgDueDate
     */
    public String getOrgDueDate()
    {
        return orgDueDate;
    }

    /**
     * <p>
     * Setting value for orgDueDate.
     * </p>
     * 
     * @param orgDueDate
     *            the orgDueDate to set
     */
    public void setOrgDueDate(String orgDueDate)
    {
        this.orgDueDate = orgDueDate;
    }

    /**
     * <p>
     * Getter for poComments.
     * </p>
     * 
     * @return the poComments
     */
    public String getPoComments()
    {
        return poComments;
    }

    /**
     * <p>
     * Setting value for poComments.
     * </p>
     * 
     * @param poComments
     *            the poComments to set
     */
    public void setPoComments(String poComments)
    {
        this.poComments = poComments;
    }

    /**
     * <p>
     * Getter for responsible.
     * </p>
     * 
     * @return the responsible
     */
    public String getResponsible()
    {
        return responsible;
    }

    /**
     * <p>
     * Setting value for responsible.
     * </p>
     * 
     * @param responsible
     *            the responsible to set
     */
    public void setResponsible(String responsible)
    {
        this.responsible = responsible;
    }

    /**
     * <p>
     * Getter for mtpRef.
     * </p>
     * 
     * @return the mtpRef
     */
    public String getMtpRef()
    {
        return mtpRef;
    }

    /**
     * <p>
     * Setting value for mtpRef.
     * </p>
     * 
     * @param mtpRef
     *            the mtpRef to set
     */
    public void setMtpRef(String mtpRef)
    {
        this.mtpRef = mtpRef;
    }

    /**
     * <p>
     * Getter for status.
     * </p>
     * 
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * <p>
     * Setting value for status.
     * </p>
     * 
     * @param status
     *            the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    /**
     * <p>
     * Getter for shortName.
     * </p>
     * 
     * @return the shortName
     */
    public String getShortName()
    {
        return shortName;
    }

    /**
     * <p>
     * Setting value for shortName.
     * </p>
     * 
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    /**
     * <p>
     * Getter for program.
     * </p>
     * 
     * @return the program
     */
    public String getProgram()
    {
        return program;
    }

    /**
     * <p>
     * Setting value for program.
     * </p>
     * 
     * @param program the program to set
     */
    public void setProgram(String program)
    {
        this.program = program;
    }

    /**
     * <p>
     * Getter for description.
     * </p>
     * 
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * <p>
     * Setting value for description.
     * </p>
     * 
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * <p>
     * Getter for locations.
     * </p>
     * 
     * @return the locations
     */
    public String getLocations()
    {
        return locations;
    }

    /**
     * <p>
     * Setting value for locations.
     * </p>
     * 
     * @param locations the locations to set
     */
    public void setLocations(String locations)
    {
        this.locations = locations;
    }

    /**
     * <p>
     * Getter for webTitle.
     * </p>
     * 
     * @return the webTitle
     */
    public String getWebTitle()
    {
        return webTitle;
    }

    /**
     * <p>
     * Setting value for webTitle.
     * </p>
     * 
     * @param webTitle the webTitle to set
     */
    public void setWebTitle(String webTitle)
    {
        this.webTitle = webTitle;
    }

    /**
     * <p>
     * Getter for donors.
     * </p>
     * 
     * @return the donors
     */
    public String getDonors()
    {
        return donors;
    }

    /**
     * <p>
     * Setting value for donors.
     * </p>
     * 
     * @param donors the donors to set
     */
    public void setDonors(String donors)
    {
        this.donors = donors;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProjectOutcomeView [itemNo=");
        builder.append(itemNo);
        builder.append(", outputId=");
        builder.append(outputId);
        builder.append(", mainProjId=");
        builder.append(mainProjId);
        builder.append(", subProjectId=");
        builder.append(subProjectId);
        builder.append(", projectName=");
        builder.append(projectName);
        builder.append(", name=");
        builder.append(name);
        builder.append(", year=");
        builder.append(year);
        builder.append(", dueDate=");
        builder.append(dueDate);
        builder.append(", orgDueDate=");
        builder.append(orgDueDate);
        builder.append(", poComments=");
        builder.append(poComments);
        builder.append(", responsible=");
        builder.append(responsible);
        builder.append(", mtpRef=");
        builder.append(mtpRef);
        builder.append(", status=");
        builder.append(status);
        builder.append(", shortName=");
        builder.append(shortName);
        builder.append(", program=");
        builder.append(program);
        builder.append(", description=");
        builder.append(description);
        builder.append(", locations=");
        builder.append(locations);
        builder.append(", webTitle=");
        builder.append(webTitle);
        builder.append(", donors=");
        builder.append(donors);
        builder.append("]");
        return builder.toString();
    }

	/**
	 * <p>
	 * Getter for outputName.
	 * </p>
	 * 
	 * @return the outputName
	 */
	public String getOutputName() {
		return outputName;
	}

	/**
	 * <p>
	 * Setting value for outputName.
	 * </p>
	 * 
	 * @param outputName the outputName to set
	 */
	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

}
