/*
 * FILENAME
 *     ProjectOutcomeQueryDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao;
import com.hashcode.iwmi.cgmaps.dao.ProjectOutcomeQueryDao;
import com.hashcode.iwmi.cgmaps.model.ProjectOutcomeView;
import com.hashcode.iwmi.cgmaps.services.AdvancedSearchCriteria;
import com.hashcode.iwmi.cgmaps.services.ProjectOutcomeRowMapper;
import com.hashcode.iwmi.cgmaps.services.SearchCriteria;
import com.hashcode.iwmi.cgmaps.util.Utilities;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation of the Project outcome dao service.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 **/
@Resource(name = "ProjectOutcomeQueryDao")
public class ProjectOutcomeQueryDaoImpl extends JdbcDaoSupport implements ProjectOutcomeQueryDao
{

    private static final Logger logger = LoggerFactory.getLogger(ProjectOutcomeQueryDaoImpl.class);

    private String projectDetailViewName = "pr_details";
    private String projectOutcomeViewName = "pr_output";
    private String countryDtlViewName = "pr_countries";
    
	private Map<String, String> programFilterInput = new HashMap<String, String>();

	private Map<String, String> allProgramsYearMap = new HashMap<String, String>();
	
	@Autowired
	private ProjectInfoQueryDao projectInfoQueryDao;
	
	/**
	 * <p>
	 * Populate the Program filter by year
	 * </p>
	 */
	@PostConstruct
	protected void initializeProgramFilterMapByYear() {
		allProgramsYearMap.clear();
		List<String> programs = projectInfoQueryDao.fetchAllPrograms();

		for (String program : programs) {
			String year = programFilterInput.get(program);

			if (!Utilities.isNullOrEmpty(year)) {
				allProgramsYearMap.put(program, year);
			} else {
				allProgramsYearMap.put(program, "");
			}
		}
	}

    public List<String> getProgramsForCriteria(SearchCriteria criteria)
    {
        logger.debug("Before loading the project outcome programs from data source");

        StringBuilder projectOutcomeProgramSQL =
            new StringBuilder("SELECT pd.Program pdProg FROM " + projectOutcomeViewName + " p ");
        projectOutcomeProgramSQL.append(" INNER JOIN ").append(projectDetailViewName).append(" pd ");
        projectOutcomeProgramSQL.append(" ON p.SubProjectId = pd.SubProjectId ");

        if (criteria != null && criteria.hasCriteria())
        {
            AdvancedSearchCriteria advCriteria = (AdvancedSearchCriteria) criteria;
            if (!advCriteria.getCountries().isEmpty() || !advCriteria.getRegions().isEmpty())
            {
                projectOutcomeProgramSQL.append(" INNER JOIN  ").append(countryDtlViewName).append(" c ");
                projectOutcomeProgramSQL.append(" ON p.SubProjectId = c.SubProjectId ");
            }

            projectOutcomeProgramSQL.append(" WHERE " + advCriteria.getCriteriaQuery(allProgramsYearMap));
        }

        projectOutcomeProgramSQL.append(" GROUP BY pd.Program ORDER BY pd.Program ASC");

        List<String> outcomeView = getJdbcTemplate().query(projectOutcomeProgramSQL.toString(), new RowMapper<String>()
        {

            public String mapRow(ResultSet rs, int rowNum) throws SQLException
            {
                return rs.getString("pdProg");
            }
        });

        if (outcomeView == null || outcomeView.isEmpty())
        {
            logger.info("No Project outcome were found.");
            outcomeView = new ArrayList<String>();
        }

        logger.info("Project outcome list size : {}", outcomeView.size());
        return outcomeView;

    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.cgmaps.dao.ProjectOutcomeQueryDao#fetchProjectOutcomeListView(com.hashcode.iwmi.cgmaps.services.SearchCriteria)
     */
    public List<ProjectOutcomeView> fetchProjectOutcomeListView(SearchCriteria criteria)
    {
        logger.debug("Before loading the project outcomes from data source");

        final StringBuffer fieldList = new StringBuffer();

        fieldList
            .append(" p.ItemNo ItemNo, p.OutputId OutputId, p.SubProjectID SubProjectID, p.SubProjectName SubProjectName, "
                + " p.Year Year, p.Name Name, p.DueDate DueDate, p.OrgDueDate OrgDueDate, p.POComments POComments, "
                + " p.Responsible Responsible, p.MTPRef MTPRef, p.Status Status, ");

        fieldList.append(" pd.ProjectShortName pdShrtName ," + " pd.Description pdDesc ," + " pd.Donors pdDonors ,"
            + " pd.Locations pdLoc ," + " pd.Program pdProg ," + " pd.WebTitle pdWebTitle ,"
            + " pd.startDate pdStartDate ," + " pd.endDate pdEndDate ");

        StringBuilder projectOutcomeSQL =
            new StringBuilder("SELECT " + fieldList.toString() + " FROM " + projectOutcomeViewName + " p ");
        projectOutcomeSQL.append(" INNER JOIN ").append(projectDetailViewName).append(" pd ");
        projectOutcomeSQL.append(" ON p.SubProjectId = pd.SubProjectId ");

        if (criteria != null && criteria.hasCriteria())
        {
            AdvancedSearchCriteria advCriteria = (AdvancedSearchCriteria) criteria;
            if (!advCriteria.getCountries().isEmpty() || !advCriteria.getRegions().isEmpty())
            {
                projectOutcomeSQL.append(" INNER JOIN  ").append(countryDtlViewName).append(" c ");
                projectOutcomeSQL.append(" ON p.SubProjectId = c.SubProjectId ");
            }

            projectOutcomeSQL.append(" WHERE " + advCriteria.getCriteriaQuery(allProgramsYearMap));
        }

        // projectOutcomeSQL.append(" GROUP BY p.SubProjectId, p.MainProjectID, p.ItemNo, p.OutputId ORDER BY p.Year DESC ");
        projectOutcomeSQL.append(" ORDER BY p.Year DESC ");

        logger.info("Project outcome query : {}", projectOutcomeSQL.toString());

        List<ProjectOutcomeView> outcomeView =
            getJdbcTemplate().query(projectOutcomeSQL.toString(), new ProjectOutcomeRowMapper());

        if (outcomeView == null || outcomeView.isEmpty())
        {
            logger.info("No Project outcome were found.");
            outcomeView = new ArrayList<ProjectOutcomeView>();
        }

        logger.info("Project outcome list size : {}", outcomeView.size());
        return outcomeView;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.cgmaps.dao.ProjectOutcomeQueryDao#findProjectOutcomeView(java.lang.String)
     */
    public List<ProjectOutcomeView> findProjectOutcomeView(String projectId)
    {

        final StringBuffer fieldList = new StringBuffer();
        fieldList
            .append(" p.ItemNo ItemNo, p.OutputId OutputId, p.SubProjectID SubProjectID, p.SubProjectName SubProjectName, "
                + " p.Year Year, p.Name Name, p.DueDate DueDate, p.OrgDueDate OrgDueDate, p.POComments POComments, "
                + " p.Responsible Responsible, p.MTPRef MTPRef, p.Status Status, ");

        fieldList.append(" pd.ProjectShortName pdShrtName ," + " pd.Description pdDesc ," + " pd.Donors pdDonors ,"
            + " pd.Locations pdLoc ," + " pd.Program pdProg ," + " pd.WebTitle pdWebTitle ,"
            + " pd.startDate pdStartDate ," + " pd.endDate pdEndDate ");

        StringBuilder projectOutcomeSQL =
            new StringBuilder("SELECT " + fieldList.toString() + " FROM " + projectOutcomeViewName + " p ");
        projectOutcomeSQL.append(" INNER JOIN ").append(projectDetailViewName).append(" pd ");
        projectOutcomeSQL.append(" ON p.SubProjectId = pd.SubProjectId ");
        projectOutcomeSQL.append(" WHERE p.SubProjectId = '" + projectId + "' ");

        List<ProjectOutcomeView> outcomeView =
            getJdbcTemplate().query(projectOutcomeSQL.toString(), new ProjectOutcomeRowMapper());

        if (outcomeView == null || outcomeView.isEmpty())
        {
            logger.info("No Project outcomess were found.");
            outcomeView = new ArrayList<ProjectOutcomeView>();
        }
        return outcomeView;
    }

    /**
     * <p>
     * Setting value for projectOutcomeViewName.
     * </p>
     * 
     * @param projectOutcomeViewName
     *            the projectOutcomeViewName to set
     */
    public void setProjectOutcomeViewName(String projectOutcomeViewName)
    {
        this.projectOutcomeViewName = projectOutcomeViewName;
    }

    /**
     * <p>
     * Getter for countryDtlViewName.
     * </p>
     * 
     * @return the countryDtlViewName
     */
    public String getCountryDtlViewName()
    {
        return countryDtlViewName;
    }

    /**
     * <p>
     * Setting value for countryDtlViewName.
     * </p>
     * 
     * @param countryDtlViewName
     *            the countryDtlViewName to set
     */
    public void setCountryDtlViewName(String countryDtlViewName)
    {
        this.countryDtlViewName = countryDtlViewName;
    }

    /**
     * <p>
     * Getter for projectDetailViewName.
     * </p>
     * 
     * @return the projectDetailViewName
     */
    public String getProjectDetailViewName()
    {
        return projectDetailViewName;
    }

    /**
     * <p>
     * Setting value for projectDetailViewName.
     * </p>
     * 
     * @param projectDetailViewName
     *            the projectDetailViewName to set
     */
    public void setProjectDetailViewName(String projectDetailViewName)
    {
        this.projectDetailViewName = projectDetailViewName;
    }

	public Map<String, String> getProgramFilterInput() {
		return programFilterInput;
	}

	public void setProgramFilterInput(Map<String, String> programFilterInput) {
		this.programFilterInput = programFilterInput;
	}

}
