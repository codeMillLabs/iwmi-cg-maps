/*
 * FILENAME
 *     ProjectInfoQueryDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.dao.impl;

import static com.hashcode.iwmi.cgmaps.util.Utilities.isNullOrEmpty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao;
import com.hashcode.iwmi.cgmaps.model.ProjectInfoView;
import com.hashcode.iwmi.cgmaps.model.ProjectListView;
import com.hashcode.iwmi.cgmaps.services.AdvancedSearchCriteria;
import com.hashcode.iwmi.cgmaps.services.ProjectDataViewRowMapper;
import com.hashcode.iwmi.cgmaps.services.ProjectListCriteria;
import com.hashcode.iwmi.cgmaps.services.ProjectListViewRowMapper;
import com.hashcode.iwmi.cgmaps.services.SearchCriteria;
import com.hashcode.iwmi.cgmaps.util.Utilities;

/**
 * <p>
 * Implementation of the Project Info query Dao.
 * </p>
 * 
 * @author Amila Silva
 * @contact amila@hashcodesys.com
 * @version 1.0
 * 
 */
@Resource(name = "ProjectInfoQueryDao")
public class ProjectInfoQueryDaoImpl extends JdbcDaoSupport implements ProjectInfoQueryDao
{

    private static final Logger logger = LoggerFactory.getLogger(ProjectInfoQueryDaoImpl.class);

    private String projectDetailViewName = "pr_details";
    private String countryDtlViewName = "pr_countries";
    private Map<String, String> programFilterInput = new HashMap<String, String>();
    
    private Map<String, String> allProgramsYearMap = new HashMap<String, String>();

	/**
	 * <p>
	 * Populate the Program filter by year
	 * </p>
	 */
	@PostConstruct
	protected void initializeProgramFilterMapByYear() {
		allProgramsYearMap.clear();
		List<String> programs = fetchAllPrograms();

		for (String program : programs) {
			String year = programFilterInput.get(program);

			if (!Utilities.isNullOrEmpty(year)) {
				allProgramsYearMap.put(program, year);
			} else {
				allProgramsYearMap.put(program, "");
			}
		}
	}
    
    /**
     * {@inheritDoc}
     * 
     */
    public int getProjectListCountForCriteria(SearchCriteria criteria)
    {
        logger.debug("Fetch Project List count - started");
        initializeProgramFilterMapByYear();
        
        ProjectListCriteria projCriteria = (ProjectListCriteria) criteria;
        String query = getProjectListViewQuery(projCriteria, true);	
         
        int totalCount = getJdbcTemplate().queryForInt(query);

        logger.debug("Fetch Project List count - end");
        return totalCount;
    }

    /**
     * {@inheritDoc}
     */
    public List<ProjectListView> fetchProjectListView(SearchCriteria criteria)
    {
        logger.debug("Fetch Project List view - started");
        initializeProgramFilterMapByYear();
        ProjectListCriteria projCriteria = (ProjectListCriteria) criteria;

        String query = getProjectListViewQuery(projCriteria, false);
        logger.info("Fetch Project Info view - Query [ {} ]", query);

        List<ProjectListView> projects = getJdbcTemplate().query(query, new ProjectListViewRowMapper());

        if (projects == null || projects.isEmpty())
        {
            logger.info("No Project details were found.");
        }
        logger.debug("Fetch Project List view - end");
        return projects;
    }

    private String getProjectListViewQuery(ProjectListCriteria criteria, boolean isCountQuery)
    {

        StringBuilder queryBuilder = new StringBuilder();
        if (isCountQuery)
        {
            queryBuilder.append("SELECT COUNT(pd.SubProjectId) FROM ").append(projectDetailViewName).append(" pd ");
        }
        else
        {
            queryBuilder.append("SELECT pd.* FROM ").append(projectDetailViewName).append(" pd ");
        }
        if (criteria != null && criteria.hasCriteria())
        {
            if (!isNullOrEmpty(criteria.getCountry()))
            {
                queryBuilder.append(" LEFT JOIN  ").append(countryDtlViewName).append(" c ");
                queryBuilder.append(" ON pd.SubProjectId = c.SubProjectId ");
            }

            queryBuilder.append(" WHERE ").append(criteria.getCriteriaQuery(allProgramsYearMap));
        } 

        logger.debug("Project List view query. {}", queryBuilder.toString());
        return queryBuilder.toString();
    }

    /**
     * @param projectDetailViewName
     *            the projectDetailViewName to set
     */
    public void setProjectDetailViewName(String projectDetailViewName)
    {
        this.projectDetailViewName = projectDetailViewName;
    }

    public List<ProjectInfoView> fetchProjectInfo(AdvancedSearchCriteria criteria)
    {
        logger.debug("Fetch Project Info view - started");
        initializeProgramFilterMapByYear();
        
        String query = getProjectInfoViewQuery(criteria);
        logger.info("Fetch Project Info view - Query [ {} ]", query);

        List<ProjectInfoView> projects = getJdbcTemplate().query(query, new ProjectDataViewRowMapper());

        if (projects == null || projects.isEmpty())
        {
            logger.info("No Project details were found.");
            projects = new ArrayList<ProjectInfoView>();
        }
        logger.info("Fetch Project Info view - end, [ Total Projects : {}]", projects.size());
        return projects;
    }

    public List<String> getProgramsForProjectCriteria(AdvancedSearchCriteria criteria)
    {
        logger.debug("Fetch Programs for the projects in criteria - started");
        initializeProgramFilterMapByYear();
        
        String query = getProjectProgramQuery(criteria);
        List<String> programList = getJdbcTemplate().query(query, new RowMapper<String>()
        {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException
            {
                return rs.getString("pdProg");
            }

        });

        if (programList == null || programList.isEmpty())
        {
            logger.info("No Project details were found.");
            programList = new ArrayList<String>();
        }
        logger.debug("Fetch Programs for the projects in criteria - end, [ Total Programs for criteria : {}]",
            programList.size());
        return programList;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao#fingProjectById(java.lang.Long)
     */
    public ProjectInfoView fingProjectById(String projectId)
    {
        logger.debug("Find Project Info By Id - started");
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT pd.*, c.geo_region_name geo_region_name FROM ").append(projectDetailViewName)
            .append(" pd ");
        queryBuilder.append(" LEFT JOIN  ").append(countryDtlViewName).append(" c ");
        queryBuilder.append(" ON pd.SubProjectId = c.SubProjectId ");
        queryBuilder.append(" WHERE pd.SubProjectId = '" + projectId + "'");
        
        logger.debug("Find Project By Id query : {}", queryBuilder.toString());

        List<ProjectInfoView> projectDetail =
            getJdbcTemplate().query(queryBuilder.toString(), new ProjectDataViewRowMapper());

        if (projectDetail == null || projectDetail.isEmpty())
        {
            logger.info("No Project detail information found for Id : {}", projectId);
        }
        else
        {
            return projectDetail.get(0);
        }
        logger.debug("Find Project Info By Id - end");
        return null;
    }

    private String getProjectInfoViewQuery(AdvancedSearchCriteria criteria)
    {

        StringBuilder queryBuilder = new StringBuilder();

        queryBuilder.append("SELECT  pd.*, c.geo_region_name geo_region_name  FROM ").append(projectDetailViewName)
            .append(" pd ");
        queryBuilder.append(" LEFT JOIN  ").append(countryDtlViewName).append(" c ");
        queryBuilder.append(" ON pd.SubProjectId = c.SubProjectId ");

        if (criteria.hasCriteria())
        {
            queryBuilder.append(" WHERE ").append(criteria.getCriteriaQuery(allProgramsYearMap));
        }
        queryBuilder.append(" GROUP BY pd.SubProjectId ");

        logger.debug("Project List view, Advance Search query. {}", queryBuilder.toString());
        return queryBuilder.toString();
    }

    private String getProjectProgramQuery(AdvancedSearchCriteria criteria)
    {

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT pd.Program pdProg FROM ").append(projectDetailViewName).append(" pd ");

        if (criteria.hasCriteria())
        {
            if (!criteria.getCountries().isEmpty() || !criteria.getRegions().isEmpty())
            {
                queryBuilder.append(" LEFT JOIN  ").append(countryDtlViewName).append(" c ");
                queryBuilder.append(" ON pd.SubProjectId = c.SubProjectId ");
            }
            queryBuilder.append(" WHERE ").append(criteria.getCriteriaQuery(allProgramsYearMap));
        }

        queryBuilder.append(" GROUP BY pd.Program ORDER BY pd.Program ASC");

        logger.debug("Project List view query. {}", queryBuilder.toString());
        return queryBuilder.toString();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao#fetchAllAvailbaleCountries()
     */
    public List<String> fetchAllAvailbaleCountries()
    {
        final String fetchAllCountriesSQL =
            "SELECT DISTINCT c.country_name country FROM " + countryDtlViewName + " c ORDER BY c.country_name ASC";
        List<String> countries = getJdbcTemplate().query(fetchAllCountriesSQL, new RowMapper<String>()
        {

            public String mapRow(ResultSet rs, int index) throws SQLException
            {
                return rs.getString("country");
            }

        });

        if (countries == null || countries.isEmpty())
        {
            logger.info("No countries found");
            countries = new ArrayList<String>();
        }
        return getListWithoutNullValues(countries);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao#fetchCountriesForRegion(java.util.List)
     */
    public List<String> fetchCountriesForRegion(List<String> regions)
    {
        String regionsFields = "";
        if (!regions.isEmpty())
        {
            regionsFields = " WHERE ";
            for (int i = 0; i < regions.size(); i++)
            {
                regionsFields += " c.geo_region_name = '" + regions.get(i) + "' ";

                if (i < regions.size() - 1)
                {
                    regionsFields += " OR ";
                }
            }
        }

        final String fetchCountriesByRegionsSQL =
            "SELECT DISTINCT c.country_name country FROM " + countryDtlViewName + " c " + regionsFields
                + " ORDER BY c.country_name ASC ";
        
        logger.debug("Fetch countries by regions query : {}", fetchCountriesByRegionsSQL);
        List<String> countries = getJdbcTemplate().query(fetchCountriesByRegionsSQL, new RowMapper<String>()
        {

            public String mapRow(ResultSet rs, int index) throws SQLException
            {
                return rs.getString("country");
            }

        });

        if (countries == null || countries.isEmpty())
        {
            logger.info("No countries found");
            countries = new ArrayList<String>();
        }
        return getListWithoutNullValues(countries);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao#fetchAllPrograms()
     */
    public List<String> fetchAllPrograms()
    {
        final String fetchAllProgramsSQL =
            "SELECT DISTINCT p.Program Program FROM " + projectDetailViewName + " p ORDER BY  p.Program ASC";
        List<String> programs = getJdbcTemplate().query(fetchAllProgramsSQL, new RowMapper<String>()
        {

            public String mapRow(ResultSet rs, int index) throws SQLException
            {
                return rs.getString("Program");
            }

        });

        if (programs == null || programs.isEmpty())
        {
            logger.info("No countries found");
            programs = new ArrayList<String>();
        }
        return getListWithoutNullValues(programs);
    }

    public int getProjectCountByCountry(final String countryName)
    {
        final String getProjectCountSQL;
        if (countryName != null && !countryName.isEmpty())
        {
            getProjectCountSQL =
                "SELECT COUNT(p.SubProjectId) FROM " + projectDetailViewName + " p " + " INNER JOIN "
                    + countryDtlViewName + " c ON c.SubProjectId = p.SubProjectId " + " WHERE c.country_name = ?";
            return getJdbcTemplate().queryForInt(getProjectCountSQL, countryName);
        }
        else
        {
            getProjectCountSQL =
                "SELECT COUNT(p.SubProjectId) FROM " + projectDetailViewName + " p " + " INNER JOIN "
                    + countryDtlViewName + "  c ON c.SubProjectId = p.SubProjectId ";
            return getJdbcTemplate().queryForInt(getProjectCountSQL);
        }

    }

    public Map<String, Integer> getProjectsForCountries(SearchCriteria criteria)
    {
        final Map<String, Integer> countryCountMap = new HashMap<String, Integer>();

        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT c.country_name country_name, count(c.country_name) c_count  FROM " + countryDtlViewName
            + " c ");
        builder.append(" INNER JOIN " + projectDetailViewName + " pd ");
        builder.append(" ON pd.SubProjectId = c.SubProjectId ");

        if (criteria != null && criteria.hasCriteria())
        {
            ProjectListCriteria listCriteria = (ProjectListCriteria) criteria;
            builder.append(" WHERE ").append(listCriteria.getCriteriaQuery(allProgramsYearMap));
        }

        builder.append(" group by c.country_name ");

        getJdbcTemplate().query(builder.toString(), new RowMapper<String>()
        {

            public String mapRow(ResultSet rs, int index) throws SQLException
            {
                String country = rs.getString("country_name");
                int count = rs.getInt("c_count");
                countryCountMap.put(country, count);
                return null;
            }

        });

        logger.info("Project count for Countries, Map size : ", countryCountMap.size());
        return countryCountMap;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao#fetchAllRegions()
     */
    public List<String> fetchAllRegions()
    {
        final String fetchAllProgramsSQL =
            "SELECT DISTINCT c.geo_region_name geo_region_name FROM " + countryDtlViewName
                + " c ORDER BY c.geo_region_name ASC ";
        List<String> regions = getJdbcTemplate().query(fetchAllProgramsSQL, new RowMapper<String>()
        {
            public String mapRow(ResultSet rs, int index) throws SQLException
            {
                return rs.getString("geo_region_name");
            }
        });

        if (regions == null || regions.isEmpty())
        {
            logger.info("No Regions found");
            regions = new ArrayList<String>();
        }

        logger.debug("Total available regions : {}", regions.size());
        return getListWithoutNullValues(regions);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao#fetchAllDonors()
     */
    public List<String> fetchAllDonors()
    {
        final String fetchAllProgramsSQL = "SELECT DISTINCT p.Donors Donors FROM " + projectDetailViewName + " p ";
        List<String> donors = getJdbcTemplate().query(fetchAllProgramsSQL, new RowMapper<String>()
        {
            public String mapRow(ResultSet rs, int index) throws SQLException
            {
                return rs.getString("Donors");
            }
        });

        Set<String> donorsSet = new HashSet<String>();
        if (null != donors && !donors.isEmpty())
        {

            for (String donorStr : donors)
            {
                if (donorStr == null)
                    continue;
                String[] donorArr = donorStr.split(",", -1);
                for (String dnr : donorArr)
                {
                    if (null != dnr)
                    {
                        donorsSet.add(dnr.trim());
                    }
                }
            }
        }

        donors = getListWithoutNullValues(donorsSet);
        Collections.sort(donors);
        logger.debug("Total available donors : {}", donors.size());
        return donors;
    }

    /**
     * {@inheritDoc}
     * 
     */
    public List<Integer> getProjectStartedYr()
    {

        final String fetchProjectStartedYrsSQL =
            "SELECT pd.StartDate startDate FROM " + projectDetailViewName
                + "  pd GROUP BY pd.StartDate ORDER BY pd.StartDate DESC";

        final Set<Integer> projectStarts = new HashSet<Integer>();

        getJdbcTemplate().query(fetchProjectStartedYrsSQL, new RowMapper<String>()
        {

            @SuppressWarnings("null")
            public String mapRow(ResultSet rs, int index) throws SQLException
            {

                String value = rs.getString("startDate");
                if (value != null || !value.isEmpty())
                {
                    value = value.substring(0, 4);
                    if (value != null)
                        projectStarts.add(Integer.valueOf(value));
                }
                return "";
            }

        });

        List<Integer> refineByYears = new ArrayList<Integer>(projectStarts);
        Collections.sort(refineByYears, Collections.reverseOrder());
        return refineByYears;
    }

    private List<String> getListWithoutNullValues(final Collection<String> valueList)
    {
        List<String> nonNullValueList = new ArrayList<String>();

        for (String value : valueList)
        {
            if (value != null && !value.isEmpty())
            {
                nonNullValueList.add(value.trim());
            }
        }
        return nonNullValueList;
    }

    /**
     * <p>
     * Setting value for countryDtlViewName.
     * </p>
     * 
     * @param countryDtlViewName
     *            the countryDtlViewName to set
     */
    public void setCountryDtlViewName(String countryDtlViewName)
    {
        this.countryDtlViewName = countryDtlViewName;
    }

	public Map<String, String> getProgramFilterInput() {
		return programFilterInput;
	}

	public void setProgramFilterInput(Map<String, String> programFilterInput) {
		this.programFilterInput = programFilterInput;
	}

}
