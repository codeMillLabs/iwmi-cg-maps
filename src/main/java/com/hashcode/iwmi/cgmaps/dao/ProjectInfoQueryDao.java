/*
 * FILENAME
 *     ProjectInfoQueryDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.cgmaps.dao;

import java.util.List;
import java.util.Map;

import com.hashcode.iwmi.cgmaps.model.ProjectInfoView;
import com.hashcode.iwmi.cgmaps.model.ProjectListView;
import com.hashcode.iwmi.cgmaps.services.AdvancedSearchCriteria;
import com.hashcode.iwmi.cgmaps.services.SearchCriteria;

/**
 * <p>
 * Project information query data access object.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public interface ProjectInfoQueryDao
{
    /**
     * <p>
     * Get the count of projects eligible for given search criteria.
     * </p>
     * 
     * @param criteria
     *            criteria for the query.
     * @return total no of rows
     * 
     */
    int getProjectListCountForCriteria(SearchCriteria criteria);

    /**
     * <p>
     * Fetch the project List view data for DB.
     * </p>
     * 
     * @param criteria
     *            criteria for the query.
     * @return list of Project list views
     */
    List<ProjectListView> fetchProjectListView(SearchCriteria criteria);

    /**
     * 
     * <p>
     * fetch Project Info details according to the criteria.
     * </p>
     * 
     * @param criteria
     *            advance search criteria.
     * @return List of {@link ProjectInfoView}
     * 
     */
    List<ProjectInfoView> fetchProjectInfo(AdvancedSearchCriteria criteria);

    /**
     * <p>
     * Find the project details by Id.
     * </p>
     * 
     * @param projectId
     *            project id
     * @return {@link ProjectInfoView}
     * 
     */
    ProjectInfoView fingProjectById(String projectId);

    /**
     * <p>
     * Fetch all the available countries from the system.
     * </p>
     * 
     * @return list of countries
     * 
     */
    List<String> fetchAllAvailbaleCountries();

    /**
     * <p>
     * Fetch all countries available for given regions.
     * </p>
     * 
     * @param regions
     *            regions
     * @return list of countries
     * 
     */
    List<String> fetchCountriesForRegion(List<String> regions);

    /**
     * <p>
     * Fetch All programs available in the system.
     * </p>
     * 
     * @return list of programs available
     * 
     */
    List<String> fetchAllPrograms();

    /**
     * <p>
     * Get all project count for countries.
     * </p>
     * 
     * @return Map of countries and no of projects.
     * 
     */
    Map<String, Integer> getProjectsForCountries(SearchCriteria projCriteria);

    /**
     * <p>
     * Get the project count for each country.
     * </p>
     * 
     * @param countryName
     *            name of the country, if null it search for all
     * @return project count
     * 
     */
    int getProjectCountByCountry(final String countryName);

    /**
     * <p>
     * Fetch all the regions available for the system.
     * </p>
     * 
     * @return list of regions as strings
     * 
     */
    List<String> fetchAllRegions();

    /**
     * <p>
     * Fetch project started years for search params.
     * </p>
     * 
     * @return collection of project started years
     */
    List<Integer> getProjectStartedYr();

    /**
     * <p>
     * Fetch all the donors available for system.
     * </p>
     * 
     * @return list of donors as strings
     * 
     */
    List<String> fetchAllDonors();

    /**
     * <p>
     * Retrieve available projects for programs.
     * </p>
     * 
     * @param criteria
     *            advance search criteria.
     * @return list of program names.
     * 
     */
    List<String> getProgramsForProjectCriteria(AdvancedSearchCriteria criteria);
}
