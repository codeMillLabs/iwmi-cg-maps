/*
 * FILENAME
 *     ProjectOutcomeQueryDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.dao;

import java.util.List;

import com.hashcode.iwmi.cgmaps.model.ProjectOutcomeView;
import com.hashcode.iwmi.cgmaps.services.SearchCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Project outcome related data source access handling using this interface.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
public interface ProjectOutcomeQueryDao
{
    /**
     * <p>
     * Fetch the project outcome view data.
     * </p>
     * 
     * @param criteria
     *            criteria for the query.
     * @return list of {@link ProjectOutcomeView}
     */
    List<ProjectOutcomeView> fetchProjectOutcomeListView(SearchCriteria criteria);

    /**
     * <p>
     * Find the Project outcome details.
     * </p>
     * 
     * @param projectId
     *            project id
     * @return  list of {@link ProjectOutcomeView}
     * 
     */
    List<ProjectOutcomeView> findProjectOutcomeView(String projectId);

    /**
     * <p>
     * Retrieve available program names for project outcome.
     * </p>
     * 
     * @param criteria
     *            search criteria
     * @return list of programs for project outcomes.
     * 
     */
    List<String> getProgramsForCriteria(SearchCriteria criteria);

}
