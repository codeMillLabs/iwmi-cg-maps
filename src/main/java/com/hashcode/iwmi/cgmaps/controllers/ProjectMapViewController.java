/*
 * FILENAME
 *     ProjectMapViewController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.controllers;

import static com.hashcode.iwmi.cgmaps.util.Utilities.getToYear;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao;
import com.hashcode.iwmi.cgmaps.model.ProjectListView;
import com.hashcode.iwmi.cgmaps.model.ProjectViewFormModel;
import com.hashcode.iwmi.cgmaps.services.ProjectDetailQueryService;
import com.hashcode.iwmi.cgmaps.services.ProjectListCriteria;
import com.hashcode.iwmi.cgmaps.util.Utilities;
import com.hashcode.iwmi.cgmaps.views.MapViewAjaxRequestParams;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Project Map View Controller to handle all the requests related to map view.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
@Controller
public class ProjectMapViewController
{

    private static final Logger logger = LoggerFactory.getLogger(ProjectMapViewController.class);

    private static final int COUNTRY_DETAILS_POPUP_VIEW_COUNT = 3;

    @Autowired
    private ProjectInfoQueryDao projectInfoQueryDao;

    @Autowired
    private ProjectDetailQueryService projectDetailQueryService;

    /**
     * <p>
     * Data loading to the search fields to be displayed.
     * </p>
     * 
     * @param model
     *            model to set values to be passed to view
     * @return view name
     * 
     */
    @RequestMapping(value = "/map-view", method = RequestMethod.GET)
    public ModelAndView displayProjects(ModelAndView model)
    {
        logger.info("Request came to display projects in map view");
        List<Integer> projectYrs = projectInfoQueryDao.getProjectStartedYr();
        List<String> programs = projectInfoQueryDao.fetchAllPrograms();
        List<String> countryList = projectInfoQueryDao.fetchAllAvailbaleCountries();

        int totalNoOfRows = projectInfoQueryDao.getProjectListCountForCriteria(null);
        
        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setInitalPagination(totalNoOfRows);
        List<ProjectListView> projectLists = projectInfoQueryDao.fetchProjectListView(criteria);

        model.addObject("projectYrs", projectYrs);
        model.addObject("programList", programs);
        model.addObject("countryList", countryList);
        model.addObject("projectList", projectLists);
        model.addObject("totalNoOfRows", totalNoOfRows);
        model.addObject("projListModel", new ProjectViewFormModel());
        logger.debug("Project data loaded to model");

        model.setViewName("project-map-view");

        return model;
    }

    /**
     * <p>
     * Data loading to the search fields to be displayed.
     * </p>
     * 
     * @param model
     *            model to set values to be passed to view
     * @return view name
     * 
     */
    @RequestMapping(value = "/map-view", method = RequestMethod.POST)
    public String searchProjects(@ModelAttribute("projMapModel") ProjectViewFormModel projListModel, ModelMap model)
    {
        logger.info("Request came to display projects in map view");
        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setProgram(projListModel.getProgram());
        criteria.setCountry(projListModel.getCountry());
        criteria.setFromYear(projListModel.getYear());
        criteria.setToYear(getToYear(projListModel.getYear()));

        logger.info("Project Search Criteria : {}", criteria);

        List<String> programs = projectInfoQueryDao.fetchAllPrograms();
        List<String> countryList = projectInfoQueryDao.fetchAllAvailbaleCountries();
        Map<String, Double> projectDistCountriesMap =
            projectDetailQueryService.getProjectDistributionOnCountries(criteria);

        model.put("programList", programs);
        model.put("countryList", countryList);
        model.put("projectDistMpa", projectDistCountriesMap);
        model.put("projMapModel", new ProjectViewFormModel());
        logger.debug("Project data loaded to model, No of Projects : {}", projectDistCountriesMap.size());

        return "project-map-view";
    }

    /**
     * <p>
     * Data loading to the search fields to be displayed.
     * </p>
     * 
     * @param model
     *            model to set values to be passed to view
     * @return view name
     * 
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/ajax/map-country-proj-distrib", method = RequestMethod.POST)
    public @ResponseBody
    String getProjectsDistribution(@ModelAttribute("mapView") MapViewAjaxRequestParams mapView, BindingResult result,
        HttpServletRequest request, HttpServletResponse response)
    {
        logger.info("Ajax request received to retrive project distribution details");

        logger.info("Ajax request [" + mapView.toString() + "]");

        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setProgram(mapView.getProgram());
        criteria.setCountry(mapView.getCountry());
        criteria.setFromYear(mapView.getYear());
        criteria.setToYear(getToYear(mapView.getYear()));

        logger.info("Project Search Criteria : {}", criteria);
        Map<String, Double> projectDistCountriesMap =
            projectDetailQueryService.getProjectDistributionOnCountries(criteria);
        Iterator it = projectDistCountriesMap.entrySet().iterator();

        String resultString = null;
        double distributionTotal = 0;

        while (it.hasNext())
        {
            Map.Entry pairs = (Map.Entry) it.next();
            Double value = (Double) pairs.getValue();
            distributionTotal += value;
            String countryName = Utilities.countryNameFix(String.valueOf(pairs.getKey()));

            value = value / 100;

            if (value <= 0.05)
            {
                value = 0.5;
            }
            else if (value <= 0.1)
            {
                value = 0.6;
            }
            else if (value > 0.1 && value <= 0.5)
            {
                value = 0.8;
            }
            else if (value > 0.6)
            {
                value = 0.9;
            }

            String distributionValue = "0";
            distributionValue = String.valueOf((value));

            if (null == resultString)
            {
                resultString = countryName + "_" + distributionValue;
            }
            else
            {
                resultString = resultString + "|" + countryName + "_" + distributionValue;
            }
        }
        logger.info("Response String : {} | Total : {}", resultString, distributionTotal);

        return null == resultString ? "" : resultString;
    }

    /**
     * <p>
     * Data loading to the search fields to be displayed.
     * </p>
     * 
     * @param model
     *            model to set values to be passed to view
     * @return view name
     * 
     */
    @RequestMapping(value = "/ajax/map-country-country-details", method = RequestMethod.GET)
    public @ResponseBody
    String getCountryDetails(@ModelAttribute("mapView") MapViewAjaxRequestParams mapView, BindingResult result,
        HttpServletRequest request, HttpServletResponse response)
    {
        logger.info("Ajax request received to retrive project details");

        logger.info("Ajax request [" + mapView.toString() + "]");

        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setProgram(mapView.getProgram());
        criteria.setCountry(mapView.getCountry());
        criteria.setFromYear(mapView.getYear());
        criteria.setToYear(getToYear(mapView.getYear()));

        List<ProjectListView> projectLists = projectInfoQueryDao.fetchProjectListView(criteria);

        StringBuffer htmlOutput = new StringBuffer();
        htmlOutput.append("<div style=\"width:500px;height:300px;overflow: auto;\">");
        htmlOutput
            .append("<div style=\"height=80px;background:gray;color:white\"><p style=\"padding-left:10px\">Project List<p></div>");
        int count = 0;
        htmlOutput.append("<div style=\"font:16px Arial,Helvetica,sans-serif;font-weight:bold;\">");
        htmlOutput.append(mapView.getCountry() + "(" + projectLists.size() + " Projects)");
        htmlOutput.append("</div><br/>");

        for (ProjectListView projectListView : projectLists)
        {
            htmlOutput.append("<div>");
            htmlOutput.append("<img align=\"left\" src=\"" + projectListView.getProjectImage() + "\" width=\"150px\" height=\"100px\"/>");
            htmlOutput.append("<div width=\"60%\"><a style=\"text-decoration:none;\" href=\"prj-details?prjId="
                + projectListView.getProjectId() + "\">");
            htmlOutput.append("<div style=\"font:15px Arial,Helvetica,sans-serif;font-weight:bold;\">"
                + projectListView.getWebTitle() + "</div>");
            htmlOutput.append("</a></div>");
            htmlOutput.append("</div>");

            count++;
            if (count == COUNTRY_DETAILS_POPUP_VIEW_COUNT)
                break;
            else
                htmlOutput.append("<div style=\"clear:both\"></div><hr/>");
        }

        htmlOutput.append("</div><br/>");
        htmlOutput.append("<div>");
        htmlOutput.append("<a href=\"cntrylist-view?program=" + mapView.getProgram() + "&country="
            + mapView.getCountry() + "&year=" + mapView.getYear() + "\">View the list of all " + projectLists.size()
            + " projects in " + mapView.getCountry() + "</a>");
        htmlOutput.append("</div>");

        logger.info("Project Search Criteria : {}", criteria);

        return htmlOutput.toString();
    }

    /**
     * <p>
     * Display the projects according to the search criteria.
     * </p>
     * 
     * @param model
     *            model to set values to be passed to view
     * @return view name
     * 
     */
    @RequestMapping(value = "/cntrylist-view", method = RequestMethod.GET)
    public String viewProjectsForCountry(@RequestParam("program") String program,
        @RequestParam("country") String country, @RequestParam("year") String year, ModelMap model)
    {
        logger.info("Request came to display projects in list view");

        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setProgram(program);
        criteria.setCountry(country);
        criteria.setFromYear(year);
        criteria.setToYear(getToYear(year));

        logger.info("Project Search Criteria : {}", criteria);

        List<Integer> projectYrs = projectInfoQueryDao.getProjectStartedYr();
        List<String> programs = projectInfoQueryDao.fetchAllPrograms();
        List<String> countryList = projectInfoQueryDao.fetchAllAvailbaleCountries();
        List<ProjectListView> projectLists = projectInfoQueryDao.fetchProjectListView(criteria);

        model.put("projectYrs", projectYrs);
        model.put("programList", programs);
        model.put("countryList", countryList);
        model.put("projectList", projectLists);
        model.addAttribute("projListModel", new ProjectViewFormModel());
        logger.debug("Project data loaded to model, No of Projects : {}", projectLists.size());

        return "project-list";
    }
}
