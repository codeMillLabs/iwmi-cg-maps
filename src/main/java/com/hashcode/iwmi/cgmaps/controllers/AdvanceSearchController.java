/*
 * FILENAME
 *     AdvanceSearchController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.controllers;

import static com.hashcode.iwmi.cgmaps.services.SearchCriteriaType.OUTCOME_SEARCH;
import static com.hashcode.iwmi.cgmaps.services.SearchCriteriaType.PROJECT_SEARCH;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao;
import com.hashcode.iwmi.cgmaps.dao.ProjectOutcomeQueryDao;
import com.hashcode.iwmi.cgmaps.model.AdvanceSearchFormModel;
import com.hashcode.iwmi.cgmaps.model.ProjectInfoView;
import com.hashcode.iwmi.cgmaps.model.ProjectOutcomeView;
import com.hashcode.iwmi.cgmaps.services.AdvancedSearchCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Advance search form controller. This class will control all the advance
 * search form requests.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
@Controller
public class AdvanceSearchController {
	private static final Logger logger = LoggerFactory
			.getLogger(AdvanceSearchController.class);

	@Autowired
	private ProjectInfoQueryDao projectInfoQueryDao;

	@Autowired
	private ProjectOutcomeQueryDao projectOutcomeQueryDao;

	/**
	 * <p>
	 * Show projects advance search form.
	 * </p>
	 * 
	 * @param model
	 *            {@link ModelAndView}
	 * @return {@link ModelAndView}
	 * 
	 */
	@RequestMapping(value = "/advn-project-form", method = RequestMethod.GET)
	public ModelAndView showProjectSearchForm(ModelAndView model) {
		logger.info("Request came to show project advance search form");

		List<Integer> projectYrs = projectInfoQueryDao.getProjectStartedYr();
		List<String> programs = projectInfoQueryDao.fetchAllPrograms();
		List<String> countries = projectInfoQueryDao.fetchAllAvailbaleCountries();
		List<String> regions = projectInfoQueryDao.fetchAllRegions();
		List<String> donors = projectInfoQueryDao.fetchAllDonors();

		model.addObject("projectYrs", projectYrs);
		model.addObject("programItmes", programs);
		model.addObject("countryItems", countries);
		model.addObject("regionItems", regions);
		model.addObject("donorItems", donors);
		model.addObject("advSerFrmModel", new AdvanceSearchFormModel());

		model.setViewName("project-advance-search-form");
		return model;
	}

	/**
	 * <p>
	 * Display projects search results advance search form.
	 * </p>
	 * 
	 * @param model
	 *            {@link ModelAndView}
	 * @return page view
	 * 
	 */
	@RequestMapping(value = "/advn-project-form", method = RequestMethod.POST)
	public String dispalyProjectSearch(
			@ModelAttribute("advSerFrmModel") AdvanceSearchFormModel searchFormModel,
			HttpServletRequest request, ModelMap model) {
		logger.info("Request came to display project search results");

		AdvancedSearchCriteria criteria = new AdvancedSearchCriteria();
		criteria.setCountries(getAsList(searchFormModel.getCountries()));
		criteria.setDonors(getAsList(searchFormModel.getDonors()));
		criteria.setPrograms(getAsList(searchFormModel.getPrograms()));
		criteria.setRegions(getAsList(searchFormModel.getRegions()));
		criteria.setStartDate(searchFormModel.getStartDate());
		criteria.setTextSearch(searchFormModel.getTextSearch());
		criteria.setSearchCriteriaType(PROJECT_SEARCH);

		logger.info("Advance project search criteria : {}", criteria);

		List<String> programList = projectInfoQueryDao
				.getProgramsForProjectCriteria(criteria);

		List<ProjectInfoView> projectInfos = projectInfoQueryDao
				.fetchProjectInfo(criteria);
		Map<String, List<ProjectInfoView>> getProjectInfoMap = getProjectInfoMap(projectInfos);

		model.put("isNoData", projectInfos.isEmpty());
		model.put("programs", programList);
		model.put("dataSize", projectInfos.size());
		model.put("dataMap", getProjectInfoMap);
		model.put("searchFormModel", searchFormModel);
		model.put("naviAtt", 3);

		// add main search criteria to session
		request.getSession(true).setAttribute("prj_criteria", criteria);

		return "project-advance-search-data";
	}

	/**
	 * <p>
	 * 
	 * </p>
	 * 
	 * @param program
	 * @param request
	 * @param model
	 * @return
	 * 
	 */
	@RequestMapping(value = "/advn-project-data", method = RequestMethod.GET)
	public String filterProjectsByProgram(@RequestParam("prog") String program,
			HttpServletRequest request, ModelMap model) {
		logger.info("Request came to display project search results");

		AdvancedSearchCriteria criteria = (AdvancedSearchCriteria) request
				.getSession().getAttribute("prj_criteria");

		logger.info("Advance project search criteria : {}", criteria);

		if (criteria == null) {
			criteria = new AdvancedSearchCriteria();
		}
		List<String> programList = projectInfoQueryDao
				.getProgramsForProjectCriteria(criteria);

		if (program != null) {
			criteria.getPrograms().clear();
			criteria.getPrograms().add(program);
		}

		List<ProjectInfoView> projectInfos = projectInfoQueryDao
				.fetchProjectInfo(criteria);
		Map<String, List<ProjectInfoView>> getProjectInfoMap = getProjectInfoMap(projectInfos);

		model.put("isNoData", projectInfos.isEmpty());
		model.put("programs", programList);
		model.put("dataSize", projectInfos.size());
		model.put("dataMap", getProjectInfoMap);

		return "project-advance-search-data";
	}

	private Map<String, List<ProjectInfoView>> getProjectInfoMap(
			List<ProjectInfoView> projectInfos) {
		Map<String, List<ProjectInfoView>> dataMap = new HashMap<String, List<ProjectInfoView>>();

		for (ProjectInfoView infoView : projectInfos) {
			List<ProjectInfoView> dataList = dataMap.get(infoView.getProgram());

			if (dataList == null) {
				dataList = new ArrayList<ProjectInfoView>();
				dataMap.put(infoView.getProgram(), dataList);
			}

			dataList.add(infoView);
		}
		return dataMap;
	}

	private List<String> getAsList(final String[] values) {
		List<String> valueList = new ArrayList<String>();

		for (String value : values) {
			valueList.add(value);
		}
		return valueList;
	}

	/**
	 * <p>
	 * Show projects outcome advance search form.
	 * </p>
	 * 
	 * @param model
	 *            {@link ModelAndView}
	 * @return {@link ModelAndView}
	 * 
	 */
	@RequestMapping(value = "/advn-outcome-form", method = RequestMethod.GET)
	public ModelAndView showProjectOutcomeSearchForm(ModelAndView model) {
		logger.info("Request came to show project advance search form");
		List<Integer> projectYrs = projectInfoQueryDao.getProjectStartedYr();
		List<String> programs = projectInfoQueryDao.fetchAllPrograms();
		List<String> countries = projectInfoQueryDao
				.fetchAllAvailbaleCountries();
		List<String> regions = projectInfoQueryDao.fetchAllRegions();

		model.addObject("projectYrs", projectYrs);
		model.addObject("programItmes", programs);
		model.addObject("countryItems", countries);
		model.addObject("regionItems", regions);
		model.addObject("advSerFrmModel", new AdvanceSearchFormModel());

		model.setViewName("outcome-advance-search-form");
		return model;
	}

	/**
	 * <p>
	 * Display projects outcome advance search form.
	 * </p>
	 * 
	 * @param model
	 *            {@link ModelAndView}
	 * @return {@link ModelAndView}
	 * 
	 */
	@RequestMapping(value = "/advn-outcome-form", method = RequestMethod.POST)
	public String dispalyProjectOutcomeSearch(
			@ModelAttribute("advSerFrmModel") AdvanceSearchFormModel searchFormModel,
			HttpServletRequest request, ModelMap model) {
		logger.info("Request came to display project outcome advance search data");
		AdvancedSearchCriteria criteria = new AdvancedSearchCriteria();
		criteria.setPrograms(getAsList(searchFormModel.getPrograms()));
		criteria.setCountries(getAsList(searchFormModel.getCountries()));
		criteria.setRegions(getAsList(searchFormModel.getRegions()));
		criteria.setSearchCriteriaType(OUTCOME_SEARCH);

		logger.info("Advance search project outcome criteria : {}", criteria);
		List<String> prgramList = projectOutcomeQueryDao
				.getProgramsForCriteria(criteria);

		List<ProjectOutcomeView> projectOutcomeViews = projectOutcomeQueryDao
				.fetchProjectOutcomeListView(criteria);
		Map<String, List<ProjectOutcomeView>> prjOutcomeViewMap = getProjectOutcomeMap(projectOutcomeViews);

		model.put("isNoData", projectOutcomeViews.isEmpty());
		model.put("programs", prgramList);
		model.put("dataSize", projectOutcomeViews.size());
		model.put("dataMap", prjOutcomeViewMap);
		model.put("searchFormModel", searchFormModel);

		// add main criteria to session
		request.getSession(true).setAttribute("outcome_criteria", criteria);

		return "outcome-advance-search-data";
	}

	@RequestMapping(value = "/advn-outcome-data", method = RequestMethod.GET)
	public String displayProjectOutcomeFileter(
			@RequestParam("prog") String program, HttpServletRequest request,
			ModelMap model) {
		logger.info("Request came to filter the project outcomes by program");

		AdvancedSearchCriteria criteria = (AdvancedSearchCriteria) request
				.getSession().getAttribute("outcome_criteria");

		logger.info("Advance search project outcome criteria : {}", criteria);

		if (criteria == null) {
			criteria = new AdvancedSearchCriteria();
		}
		List<String> programList = projectOutcomeQueryDao
				.getProgramsForCriteria(criteria);

		if (program != null) {
			criteria.getPrograms().clear();
			criteria.getPrograms().add(program);
		}

		List<ProjectOutcomeView> projectOutcomeViews = projectOutcomeQueryDao
				.fetchProjectOutcomeListView(criteria);

		Map<String, List<ProjectOutcomeView>> prjOutcomeViewMap = getProjectOutcomeMap(projectOutcomeViews);

		model.put("isNoData", projectOutcomeViews.isEmpty());
		model.put("programs", programList);
		model.put("dataSize", projectOutcomeViews.size());
		model.put("dataMap", prjOutcomeViewMap);

		return "outcome-advance-search-data";
	}

	/**
	 * <p>
	 * Data loading to the country field according to the region selection.
	 * </p>
	 * 
	 * @param param
	 *            model to set values to be passed to view
	 * @return view name
	 * 
	 */
	@RequestMapping(value = "/ajax/regionsSel", method = RequestMethod.POST)
	public @ResponseBody
	String getCountryDetails(@RequestParam("regions") String[] regions,
			HttpServletRequest request, HttpServletResponse response) {
		logger.info("regions selected request");
		List<String> regionList = Arrays.asList(regions);
		List<String> countryList = projectInfoQueryDao
				.fetchCountriesForRegion(regionList);

		StringBuffer countryString = new StringBuffer();
		for (String country : countryList) {
			if (countryString.length() == 0) {
				countryString.append(country);
			} else {
				countryString.append(",");
				countryString.append(country);
			}
		}
		logger.info("response string [" + countryString.toString() + "]");

		return countryString.toString();
	}

	private Map<String, List<ProjectOutcomeView>> getProjectOutcomeMap(
			List<ProjectOutcomeView> projOutcomes) {
		Map<String, List<ProjectOutcomeView>> dataMap = new HashMap<String, List<ProjectOutcomeView>>();

		for (ProjectOutcomeView projOutcome : projOutcomes) {
			List<ProjectOutcomeView> dataList = dataMap.get(projOutcome
					.getProgram());

			if (dataList == null) {
				dataList = new ArrayList<ProjectOutcomeView>();
				dataMap.put(projOutcome.getProgram(), dataList);
			}

			dataList.add(projOutcome);
		}
		return dataMap;
	}

}
