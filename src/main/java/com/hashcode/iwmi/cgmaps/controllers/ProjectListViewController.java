/*
 * FILENAME
 *     ProjectListViewController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.controllers;

import static com.hashcode.iwmi.cgmaps.util.Utilities.getToYear;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hashcode.iwmi.cgmaps.dao.ProjectInfoQueryDao;
import com.hashcode.iwmi.cgmaps.model.ProjectListView;
import com.hashcode.iwmi.cgmaps.model.ProjectViewFormModel;
import com.hashcode.iwmi.cgmaps.services.ProjectListCriteria;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Request Controller for project List view related data search.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
@Controller
public class ProjectListViewController
{
    private static final Logger logger = LoggerFactory.getLogger(ProjectListViewController.class);

    @Autowired
    private ProjectInfoQueryDao projectInfoQueryDao;

    /**
     * <p>
     * Data loading to the search fields to be displayed.
     * </p>
     * 
     * @param model
     *            model to set values to be passed to view
     * @return view name
     * 
     */
    @RequestMapping(value = "/list-view", method = RequestMethod.GET)
    public ModelAndView displayProjects(HttpServletRequest request, ModelAndView model)
    {
        logger.info("Request came to display projects in list view");
        List<Integer> projectYrs = projectInfoQueryDao.getProjectStartedYr();
        List<String> programs = projectInfoQueryDao.fetchAllPrograms();
        List<String> countryList = projectInfoQueryDao.fetchAllAvailbaleCountries();

        ProjectListCriteria criteria = (ProjectListCriteria) request.getSession().getAttribute("list_criteria");

        if (null == criteria)
        {
            criteria = new ProjectListCriteria();
        }

        int totalNoOfRows = projectInfoQueryDao.getProjectListCountForCriteria(criteria);

        criteria.setInitalPagination(totalNoOfRows);

        String pageNo = request.getParameter("pgNo");

        if (pageNo == null)
        {
            criteria.setCurrentPageNo(0);
        }
        else
        {
            criteria.setCurrentPageNo(Integer.valueOf(pageNo));
        }
        request.getSession().setAttribute("list_criteria", criteria);

//        Pagination pagination = new Pagination(LIST_VIEW_PAGE_SIZE);
//        pagination.setInitalPagination(totalNoOfRows);
//        pagination.setCurrentPageNo(criteria.getCurrentPageNo());
      

        List<ProjectListView> projectLists = projectInfoQueryDao.fetchProjectListView(criteria);

        model.addObject("projectYrs", projectYrs);
        model.addObject("programList", programs);
        model.addObject("countryList", countryList);
        model.addObject("projectList", projectLists);
        model.addObject("totalNoOfRows", totalNoOfRows);
//        model.addObject("pagin", pagination);
        model.addObject("projListModel", new ProjectViewFormModel());
        logger.debug("Project data loaded to model, No of Projects : {}", projectLists.size());

        model.setViewName("project-list");
        return model;
    }

    /**
     * <p>
     * Display the projects according to the search criteria.
     * </p>
     * 
     * @param model
     *            model to set values to be passed to view
     * @return view name
     * 
     */
    @RequestMapping(value = "/list-view", method = RequestMethod.POST)
    public String searchProjects(@ModelAttribute("projListModel") ProjectViewFormModel projListModel, ModelMap model)
    {
        logger.info("Request came to display projects in list view");

        ProjectListCriteria criteria = new ProjectListCriteria();
        criteria.setProgram(projListModel.getProgram());
        criteria.setCountry(projListModel.getCountry());
        criteria.setFromYear(projListModel.getYear());
        criteria.setToYear(getToYear(projListModel.getYear()));

        logger.info("Project Search Criteria : {}", criteria);

        List<Integer> projectYrs = projectInfoQueryDao.getProjectStartedYr();
        List<String> programs = projectInfoQueryDao.fetchAllPrograms();
        List<String> countryList = projectInfoQueryDao.fetchAllAvailbaleCountries();

        int totalNoOfRows = projectInfoQueryDao.getProjectListCountForCriteria(criteria);
        List<ProjectListView> projectLists = projectInfoQueryDao.fetchProjectListView(criteria);

        model.put("projectYrs", projectYrs);
        model.put("programList", programs);
        model.put("countryList", countryList);
        model.put("totalNoOfRows", totalNoOfRows);
        model.put("projectList", projectLists);
        logger.debug("Project data loaded to model, No of Projects : {}", projectLists.size());
        return "project-list";
    }
}
