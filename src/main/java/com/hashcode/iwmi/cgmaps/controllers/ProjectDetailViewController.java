/*
 * FILENAME
 *     ProjectDetailViewController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.cgmaps.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hashcode.iwmi.cgmaps.model.ProjectDetailView;
import com.hashcode.iwmi.cgmaps.services.ProjectDetailQueryService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Project detail view controller.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
@Controller
public class ProjectDetailViewController
{
    private static final Logger logger = LoggerFactory.getLogger(ProjectDetailViewController.class);

    @Autowired
    private ProjectDetailQueryService projectDetailQueryService;

    /**
     * <p>
     * Display Project Details.
     * </p>
     * 
     */
    @RequestMapping(value = "/prj-details", method = RequestMethod.GET)
    public String displayProjectDetails(@RequestParam("prjId") String projectId, Model model)
    {
        logger.info("Request came to view details of Project Id :{}", projectId);

        ProjectDetailView prjDetailView = projectDetailQueryService.findProjectDetailsById(projectId);
        boolean hasPrjData = true;
        boolean hasOutcomeData = true;

        if (prjDetailView == null || prjDetailView.getProjectInfo() == null)
        {
            hasPrjData = false;
        }

        if (prjDetailView == null || prjDetailView.getProjectOutcome().isEmpty())
        {
            hasOutcomeData = false;
        }

        model.addAttribute("hasPrjData", hasPrjData);
        model.addAttribute("hasOutcomeData", hasOutcomeData);
        model.addAttribute("prjDetail", prjDetailView.getProjectInfo());
        model.addAttribute("prjOutcome", prjDetailView.getProjectOutcome());

        logger.info("Project details successfully written to model : Project :{}", prjDetailView);
        return "project-detail-view";
    }

}
